﻿namespace app_agenda_db
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSQL = new System.Windows.Forms.Label();
            this.labelQuery = new System.Windows.Forms.Label();
            this.textBoxSQL = new System.Windows.Forms.TextBox();
            this.textBoxQueryDefinition = new System.Windows.Forms.TextBox();
            this.dataGridViewQuery = new System.Windows.Forms.DataGridView();
            this.buttonExcuteQuery = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQuery)).BeginInit();
            this.SuspendLayout();
            // 
            // labelSQL
            // 
            this.labelSQL.AutoSize = true;
            this.labelSQL.BackColor = System.Drawing.SystemColors.Control;
            this.labelSQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSQL.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelSQL.Location = new System.Drawing.Point(1149, 589);
            this.labelSQL.Name = "labelSQL";
            this.labelSQL.Size = new System.Drawing.Size(98, 20);
            this.labelSQL.TabIndex = 11;
            this.labelSQL.Text = "SQL query";
            // 
            // labelQuery
            // 
            this.labelQuery.AutoSize = true;
            this.labelQuery.BackColor = System.Drawing.SystemColors.Control;
            this.labelQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuery.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelQuery.Location = new System.Drawing.Point(833, 25);
            this.labelQuery.Name = "labelQuery";
            this.labelQuery.Size = new System.Drawing.Size(142, 20);
            this.labelQuery.TabIndex = 10;
            this.labelQuery.Text = "Query definition";
            // 
            // textBoxSQL
            // 
            this.textBoxSQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSQL.Location = new System.Drawing.Point(94, 612);
            this.textBoxSQL.Multiline = true;
            this.textBoxSQL.Name = "textBoxSQL";
            this.textBoxSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxSQL.Size = new System.Drawing.Size(1169, 160);
            this.textBoxSQL.TabIndex = 9;
            // 
            // textBoxQueryDefinition
            // 
            this.textBoxQueryDefinition.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQueryDefinition.Location = new System.Drawing.Point(94, 12);
            this.textBoxQueryDefinition.Multiline = true;
            this.textBoxQueryDefinition.Name = "textBoxQueryDefinition";
            this.textBoxQueryDefinition.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxQueryDefinition.Size = new System.Drawing.Size(724, 46);
            this.textBoxQueryDefinition.TabIndex = 8;
            // 
            // dataGridViewQuery
            // 
            this.dataGridViewQuery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewQuery.Location = new System.Drawing.Point(94, 74);
            this.dataGridViewQuery.Name = "dataGridViewQuery";
            this.dataGridViewQuery.RowTemplate.Height = 24;
            this.dataGridViewQuery.Size = new System.Drawing.Size(1169, 477);
            this.dataGridViewQuery.TabIndex = 7;
            // 
            // buttonExcuteQuery
            // 
            this.buttonExcuteQuery.Location = new System.Drawing.Point(5, 15);
            this.buttonExcuteQuery.Name = "buttonExcuteQuery";
            this.buttonExcuteQuery.Size = new System.Drawing.Size(75, 43);
            this.buttonExcuteQuery.TabIndex = 6;
            this.buttonExcuteQuery.Text = "Execute Query";
            this.buttonExcuteQuery.UseVisualStyleBackColor = true;
            this.buttonExcuteQuery.Click += new System.EventHandler(this.buttonExecuteQuery_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1349, 765);
            this.Controls.Add(this.labelSQL);
            this.Controls.Add(this.labelQuery);
            this.Controls.Add(this.textBoxSQL);
            this.Controls.Add(this.textBoxQueryDefinition);
            this.Controls.Add(this.dataGridViewQuery);
            this.Controls.Add(this.buttonExcuteQuery);
            this.Name = "MainForm";
            this.Text = "LinqToSQL Sample";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQuery)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSQL;
        private System.Windows.Forms.Label labelQuery;
        private System.Windows.Forms.TextBox textBoxSQL;
        private System.Windows.Forms.TextBox textBoxQueryDefinition;
        private System.Windows.Forms.DataGridView dataGridViewQuery;
        private System.Windows.Forms.Button buttonExcuteQuery;
    }
}


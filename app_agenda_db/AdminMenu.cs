﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app_agenda_db
{
    public partial class AdminMenu : Form
    {
        private List<Panel> panelComponents;
        private string nomeOriginale = null;
        private readonly Color[] colors = { Color.Blue, Color.Violet, Color.Red, Color.Yellow };
        private readonly int utente = 1;
        private string descrizioneObiettivoOriginale = null;

        public AdminMenu()
        {
            panelComponents = new List<Panel>();
            InitializeComponent();
            this.panelComponents.Add(attStartPanel);
            this.panelComponents.Add(pannelloAchievement);
            this.panelComponents.ForEach(p => p.Hide());
            this.comboBoxColore.DrawMode = DrawMode.OwnerDrawFixed;
            this.comboBoxColore.Items.Clear();
            foreach (Color color in colors) this.comboBoxColore.Items.Add(color);
            comboBoxColore.DrawItem += attivita_comboColore_DrawItemColore;
        }

        private void attivita_comboColore_DrawItemColore(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            // Clear the background appropriately.
            e.DrawBackground();
            int MarginHeight = 4;
            int MarginWidth = 4;
            // Draw the color sample.
            int hgt = e.Bounds.Height - 2 * MarginHeight;
            Rectangle rect = new Rectangle(
                e.Bounds.X + MarginWidth,
                e.Bounds.Y + MarginHeight,
                hgt, hgt);
            ComboBox cbo = sender as ComboBox;
            Color color = (Color)cbo.Items[e.Index];
            using (SolidBrush brush = new SolidBrush(color))
            {
                e.Graphics.FillRectangle(brush, rect);
            }

            // Outline the sample in black.
            e.Graphics.DrawRectangle(Pens.Black, rect);

            // Draw the color's name to the right.
            using (Font font = new Font(cbo.Font.FontFamily,
                cbo.Font.Size * 0.75f, FontStyle.Bold))
            {
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;
                    int x = hgt + 2 * MarginWidth;
                    int y = e.Bounds.Y + e.Bounds.Height / 2;
                    e.Graphics.TextRenderingHint =
                        System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                    e.Graphics.DrawString(color.Name, font,
                        Brushes.Black, x, y, sf);
                }
            }
            e.DrawFocusRectangle();
        }

        private void B_attivita_Click(object sender, EventArgs e)
        {
            this.panelComponents.ForEach(p => p.Hide());
            this.Attivita_rigeneraComboAttività();
            this.attStartPanel.Show();
        }

        private void B_obiettivo_Click(object sender, EventArgs e)
        {
            this.panelComponents.ForEach(p => p.Hide());
            this.RigeneraComboObiettivi();
            this.pannelloAchievement.Show();
        }

        #region attivita

        private void ResettaAttivitaBottoni()
        {
            this.button12.Enabled = false;
            this.button7.Enabled = false;
            this.button8.Enabled = false;
            this.ResettaAttivitaTesto();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            this.ResettaAttivitaBottoni();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            if (this.ControllaIntero())
            {
                var costi = new Dictionary<string, int>();
                var profitti = new Dictionary<string, int>();
                if (this.textBox4.Text.Length > 0)
                    costi.Add(Queries.VALUTA_EURO, Int32.Parse(textBox4.Text));
                if (this.textBox5.Text.Length > 0)
                    costi.Add(Queries.VALUTA_PV, Int32.Parse(textBox5.Text));
                if (this.textBox11.Text.Length > 0)
                    profitti.Add(Queries.VALUTA_EURO, Int32.Parse(textBox11.Text));
                if (this.textBox10.Text.Length > 0)
                    profitti.Add(Queries.VALUTA_PV, Int32.Parse(textBox10.Text));
                if (this.textBox12.Text.Length > 0)
                    profitti.Add(Queries.VALUTA_PP, Int32.Parse(textBox12.Text));
                Queries.SetAttivitaUtente(1, this.nomeOriginale, this.nomeTextBox.Text, this.comboBoxColore.SelectedIndex, this.descrizioneTextBox.Text, this.comboBoxCategoria.SelectedItem.ToString(), costi, profitti, -1);

                this.ResettaAttivitaBottoni();
            }
            else
            {
                MessageBox.Show("I costi devono essere interi!");
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            this.comboBoxListaAttivita.Items.Remove(this.nomeTextBox.Text);
            this.ResettaAttivitaBottoni();
        }

        private void ControllaAttivareS()
        {
            if (this.nomeTextBox.TextLength > 0 && this.descrizioneTextBox.TextLength > 0 &&
                this.textBox4.TextLength > 0 &&
                this.textBox5.TextLength > 0 && this.textBox10.TextLength > 0 &&
                this.textBox11.TextLength > 0 && this.textBox12.TextLength > 0 &&
                this.comboBoxCategoria.SelectedIndex > -1 &&
                this.comboBoxColore.SelectedIndex > -1)
            {
                this.button7.Enabled = true;
            }
            else
            {
                this.button7.Enabled = false;
            }
        }

        private void ResettaAttivitaTesto()
        {
            this.nomeTextBox.ResetText();
            this.descrizioneTextBox.ResetText();
            this.textBox4.ResetText();
            this.textBox5.ResetText();
            this.textBox6.ResetText();
            this.textBox10.ResetText();
            this.textBox11.ResetText();
            this.textBox12.ResetText();
            this.comboBoxCategoria.SelectedIndex = -1;
            this.comboBoxColore.SelectedIndex = -1;
            this.comboBoxListaAttivita.SelectedIndex = -1;
        }

        private void Attivita_rigeneraComboAttività()
        {
            var attivita = Queries.GetAttivitaUtente(1);
            this.comboBoxListaAttivita.Items.Clear();
            this.comboBoxListaAttivita.Items.AddRange(attivita.Select(a => a.nome).ToArray());
            this.comboBoxListaAttivita.ResetText();
        }

        private void Attivita_rigeneraComboCategoria()
        {
            this.comboBoxCategoria.Items.Clear();
            this.comboBoxCategoria.Items.AddRange(Queries.db.TIPOLOGIAs.Select(t => t.nome).ToArray());
            this.comboBoxCategoria.ResetText();
            this.textBox6.ResetText();
        }

        private void Attivita_FillConInfoAttivita(ATTIVITÀ attivita)
        {
            this.nomeTextBox.Text = attivita.nome;
            this.comboBoxCategoria.Text = Queries.db.CARATTERIZZAZIONEs.Where(t => t.attività == attivita.id).First().tipologia;
            this.descrizioneTextBox.Text = attivita.descrizione;
            this.comboBoxColore.SelectedIndex = Decimal.ToInt32(attivita.colore);
            var costEur = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.textBox4.Text = costEur is null ? "" : costEur.quantità.ToString();
            var costPV = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.textBox5.Text = costPV is null ? "" : costPV.quantità.ToString();
            var profEur = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.textBox11.Text = profEur is null ? "" : profEur.quantità.ToString();
            var profPV = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.textBox10.Text = profPV is null ? "" : profPV.quantità.ToString();
            var profPP = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PP).FirstOrDefault();
            this.textBox12.Text = profPP is null ? "" : profPP.quantità.ToString();
        }

        private void ComboBoxListaAttivita_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxListaAttivita.SelectedIndex > -1)
            {
                string nome = this.comboBoxListaAttivita.GetItemText(this.comboBoxListaAttivita.SelectedItem);
                this.nomeOriginale = nome;
                Attivita_FillConInfoAttivita(Queries.GetAttivitaUtentePerNome(1,nome));
                this.Attivita_rigeneraComboCategoria();
            }
            this.button8.Enabled = true;
            this.button12.Enabled = true;
        }

        private void NomeTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void DescrizioneTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private bool ControllaIntero()
        {
            int n, m;
            if (int.TryParse(this.textBox4.Text,out n) && int.TryParse(this.textBox5.Text, out m))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void TextBox5_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void ControlloAttivareAggiungi()
        {
            if (this.textBox6.TextLength > 0)
            {
                this.button13.Enabled = true;
            }
            else
            {
                this.button13.Enabled = false;
            }
        }

        private void TextBox6_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
            this.ControlloAttivareAggiungi();
        }

        private void ComboBoxColore_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void ComboBoxCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void TextBox11_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void TextBox10_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void TextBox12_TextChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControllaAttivareS();
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            Queries.AddTipologia(this.textBox6.Text);
            this.Attivita_rigeneraComboCategoria();
            this.comboBoxCategoria.SelectedIndex = -1;
            this.textBox6.ResetText();
        }

        #endregion

        #region pannello obiettivi

        bool aggiungiAttivitaClick = false;
        bool aggiungiObiettivoClick = false;
        bool aggiungiObiettivoCanBeEnabled = true;
        bool aggiungiAttivitaCanBeEnabled = true;
        bool guardaObiettiviCompletamento = false;

        private void RigeneraComboObiettivi()
        {
            var ob = Queries.GetObiettiviUtente(this.utente);
            this.obiettiviLista.Items.Clear();
            this.obiettiviLista.Items.AddRange(ob.Select(o => o.descrizione).ToArray());
        }

        private void ControlloAttivareSalva()
        {
            //controllare se tutti i campi da inserire in un nuovo obiettivo
            //sono stati inseriti: se si, attiva il bottone salva
            if (this.obiettivoDescrizioneTextBox.Text.Length > 0 &&
                this.obiettivoNVolteTextBox.Text.Length > 0 && this.obiettivoSceltaAttivitaComboBox.SelectedIndex > -1 &&
                this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex > -1 &&
                this.aggiungiAttivitaClick && this.aggiungiObiettivoClick)
            {
                this.obiettivoSalvaButton.Enabled = true;
            }
            else
            {
                this.obiettivoSalvaButton.Enabled = false;
            }
        }

        private void ControlloAttivareAggiungiAttivita()
        {
            if (this.obiettivoSceltaAttivitaComboBox.SelectedIndex > -1 && this.obiettivoNVolteTextBox.Text.Length > 0 &&
                this.aggiungiAttivitaCanBeEnabled == true)
            {
                this.obiettivoAggiungiAttivitaButton.Enabled = true;
            }
            else
            {
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
        }

        private void ControlloAttivareAggiungiObiettivo()
        {
            if (this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex > -1 && this.aggiungiObiettivoCanBeEnabled == true)
            {
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = true;
            }
            else
            {
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            }
        }

        private void ObiettivoDescrizioneTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalva();
        }

        private void ObiettivoNVolteTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalva();
            this.ControlloAttivareAggiungiAttivita();
        }

        private void ObiettivoSceltaSottoObiettivoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareAggiungiObiettivo();
            this.ControlloAttivareSalva();
        }

        private void ObiettivoSceltaAttivitaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareAggiungiAttivita();
            this.ControlloAttivareSalva();
        }

        private void ObiettivoAggiungiAttivitaButton_Click(object sender, EventArgs e)
        {
            this.aggiungiAttivitaClick = true;
            this.ControlloAttivareSalva();
        }

        private void ObiettivoAggiungiSottoObiettivoButton_Click(object sender, EventArgs e)
        {
            this.aggiungiObiettivoClick = true;
            this.ControlloAttivareSalva();
        }

        private void ResetEachText()
        {
            this.obiettivoDescrizioneTextBox.ResetText();
            this.obiettivoNVolteTextBox.ResetText();
            this.obiettivoSceltaSottoObiettivoComboBox.Items.Clear();
            this.obiettivoSceltaSottoObiettivoComboBox.ResetText();
            this.obiettivoSceltaSottoObiettivoComboBox.Items.Add("Sotto obiettivo");
            this.obiettivoSceltaAttivitaComboBox.Items.Clear();
            this.obiettivoSceltaAttivitaComboBox.ResetText();
            this.obiettivoSceltaAttivitaComboBox.Items.Add("Sotto obiettivo");
            this.obiettivoSceltaAttivitaComboBox.SelectedIndex = -1;
            this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex = -1;
            this.obiettiviLista.SelectedIndex = -1;
            this.obiettivoTermineDurataDateTimePicker.ResetText();
            this.obiettivoInizioDurataDateTimePicker.ResetText();
            this.RigeneraComboObiettivi();
        }

        private void ObiettivoAggiungiButton_Click(object sender, EventArgs e)
        {
            this.obiettivoAggiungiButton.Enabled = false;
            this.ResetEachText();
            this.obiettivoDescrizioneTextBox.Enabled = true;
            this.obiettivoNVolteTextBox.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
        }

        private void ObiettiviLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selezione = (string)this.obiettiviLista.SelectedItem;
            this.ResetEachText();
            this.descrizioneObiettivoOriginale = selezione;
            var obiettivo = Queries.GetObiettivoUtentePerNome(this.utente, selezione);
            this.obiettivoDescrizioneTextBox.Text = obiettivo.descrizione;
            this.obiettivoSceltaAttivitaComboBox.Items.AddRange(Queries.GetAttivitaUtente(this.utente).Select(a => a.nome).ToArray());
            this.obiettivoNVolteTextBox.Text = Queries.GetNVolteAttivitaPerObiettivoUtente(this.utente, this.obiettivoSceltaAttivitaComboBox.Items[0].ToString(), selezione).ToString();

            this.obiettivoAggiungiButton.Enabled = true;
            this.obiettivoSalvaButton.Enabled = true;
            this.aggiungiAttivitaClick = false;
            this.aggiungiObiettivoClick = false;
            this.obiettivoEliminaButton.Enabled = true;
            this.obiettivoInizioDurataDateTimePicker.Enabled = true;
            this.obiettivoTermineDurataDateTimePicker.Enabled = true;
            this.obiettivoAvviaButton.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
        }

        private void ObiettivoEliminaButton_Click(object sender, EventArgs e)
        {
            this.obiettivoAggiungiButton.Enabled = false;
            this.obiettiviLista.Items.Remove(this.obiettivoDescrizioneTextBox.Text);
            this.ResetEachText();
            this.obiettivoDescrizioneTextBox.Enabled = true;
            this.obiettivoNVolteTextBox.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
        }

        private void ObiettivoSalvaButton_Click(object sender, EventArgs e)
        {
            int n;
            if (int.TryParse(this.obiettivoNVolteTextBox.Text, out n))
            {
                this.obiettivoAggiungiButton.Enabled = false;
                //Queries.SetObiettivoUtente(this.utente, );
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.aggiungiAttivitaCanBeEnabled = true;
                this.obiettivoEliminaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("N volte deve essere un intero!");
            }
        }

        private void ObiettivoAvviaButton_Click(object sender, EventArgs e)
        {
            if (this.obiettivoTermineDurataDateTimePicker.Value.DayOfYear > this.obiettivoInizioDurataDateTimePicker.Value.DayOfYear)
            {
                this.obiettivoAggiungiButton.Enabled = false;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.aggiungiAttivitaCanBeEnabled = true;
                this.obiettivoEliminaButton.Enabled = false;
                this.obiettivoSalvaButton.Enabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("Valori errati");
            }
        }

        private void ObiettivoVediButton_Click(object sender, EventArgs e)
        {
            if (!this.guardaObiettiviCompletamento)
            {
                OBIETTIVO ob = new OBIETTIVO
                {
                    descrizione = "Da query"
                };
                OBBIETTIVO_COMPLETAMENTO obc = new OBBIETTIVO_COMPLETAMENTO
                {

                };
                this.guardaObiettiviCompletamento = true;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = false;
                this.obiettivoDescrizioneTextBox.Text = ob.descrizione;
                this.obiettivoNVolteTextBox.Enabled = false;
                this.aggiungiObiettivoCanBeEnabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
            else
            {
                this.guardaObiettiviCompletamento = false;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.obiettivoInizioDurataDateTimePicker.Enabled = true;
                this.obiettivoTermineDurataDateTimePicker.Enabled = true;
                this.obiettivoAvviaButton.Enabled = true;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace app_agenda_db
{
    public partial class MainForm : Form
    {
        AgendaDataClassesDataContext db = new AgendaDataClassesDataContext();
        int queryNumber = 0;
        int maxQueryNumber = 10;
        public MainForm()
        {
            InitializeComponent();
        }

        #region event 
        private void buttonExecuteQuery_Click(object sender, EventArgs e)
        {
            // gestione circolare delle query
            int q = (queryNumber++) % maxQueryNumber;
            //this.textBoxSQL.Text = q.ToString();
            switch (q)
            {
                case 0:
                    //ExecuteQuery_1();
                    break;
                case 1:
                    //ExecuteQuery_2();
                    break;
                case 2:
                    //ExecuteQuery_3();
                    break;
                case 3:
                    //ExecuteQuery_4();
                    break;
                case 4:
                   // ExecuteQuery_5();
                    break;
                case 5:
                    //ExecuteQuery_6();
                    break;
                case 6:
                    //ExecuteQuery_7();
                    break;
                case 7:
                    //ExecuteQuery_8();
                    break;
                case 8:
                    //ExecuteQuery_9();
                    break;
                case 9:
                    //ExecuteQuery_10();
                    break;
                default:
                    break;
            }
        }
        #endregion event

        #region private methods
        /// <summary>
        /// mostra il risultato della query e la sua traduzione in SQL operata dal compilatore
        /// </summary>
        /// <param name="queryResult"> risultato della query</param>
        /// <param name="queryDefinition"> definizione a parole della query</param>
        /// <param name="querySQL">traduzione in SQL </param>
        /// <param name="removeLastColum"> se true si rimuove l'ultima colonna della tabella</param>
        /// <param name="message"> se non vuoto rappresenta un messaggio da visualizzare prima di queryDefinition </param>
        private void showResults(System.Linq.IQueryable queryResult, string queryDefinition, string querySQL, bool removeLastColum, string message)
        {
            // binding della query con il dataGridViewQuery della form
            // se necessario si cancella l'ultima colonna che rappresenta il link a category
            dataGridViewQuery.Visible = true;
            dataGridViewQuery.DataSource = queryResult;
            if (removeLastColum)
                dataGridViewQuery.Columns.RemoveAt(dataGridViewQuery.Columns.Count - 1);
            textBoxQueryDefinition.Clear();
            textBoxSQL.Clear();
            if (message != null)
                queryDefinition = message + " : " + queryDefinition;
            textBoxQueryDefinition.AppendText(queryDefinition);
            textBoxSQL.AppendText(querySQL);
        }

        // queries
        private void AggiuntaAchievemnt()
        {
            // query 1:
            // dammi tutti i prodotti per cui il nome della categoria 
            // inizia con una lettera maggiore della lettera M

            /*var query = from p in db.Products
                        where String.Compare(p.Category.CategoryName, "M", true) > 0
                        select p;
            string queryDefinition = "products whose category name is greater than letter M";
            showResults(query, queryDefinition, query.ToString(), true, "query 1");*/
        }
        private void InserimentoAttivita()
        {
            // query 2:
            // dammi tutti i prodotti per cui il nome della categoria 
            // inizia con la lettera D, e prendi solo i primi 5
            /*var query = (from p in db.Products
                         where p.Category.CategoryName.StartsWith("D")
                         select p).Take(5);
            string queryDefinition = "first 5 products whose category name starts with letter D";
            showResults(query, queryDefinition, query.ToString(), true, "query 2");*/
        }

        private void VisioneAgendeAltriUtenti()
        {
            // query 3:
            // Per le categorie che contengono più di un prodotto visualizzare
            // per ciascun prodotto la quantità totale ordinata e il relativo ricavo.

            /*var query = from p in db.Products
                        where p.Category.Products.Count > 1
                        select new
                        {
                            PID = p.ProductID,
                            PN = p.ProductName,
                            TotalUnits = p.Order_Details.Sum(o => o.Quantity),
                            Revenue = p.Order_Details.Sum(o => o.UnitPrice * o.Quantity)
                        };

            string queryDefinition = "referring to categories contanining more than one product" +
                                     " for each product show total ordered quantity" +
                                     " and related revenue";
            showResults(query, queryDefinition, query.ToString(), false, "query 3");*/
        }

        private void Transazioni(String valuta)//enum tipo
        {
            // definizione della query:
            //eegue transazioni sulla valut a desiderata

            /*int orderCutoff = 20;
            var query = from c in db.Customers
                        where c.Orders.Count() > orderCutoff
                        orderby c.CustomerID
                        select new
                        {
                            c.CustomerID,
                            Name = c.ContactName,
                            OrderCount = c.Orders.Count(),
                            SumFreight = c.Orders.Sum(o => o.Freight),
                        };

            string queryDefinition = "for customers which issued more than 20 orders" +
                                     " show contact name, number of orders issued and "
                                     + "total amount of shipping expenses;" +
                                     " order the results by customer ID";
            showResults(query, queryDefinition, query.ToString(), false, "query 4");*/
        }

        private void CalcoloPercentuali(String target)
        {
            // definizione della query:
            //Per i clienti con sede a Washington visualizzare,
            //ordinati in senso decrescente per data ordine,
            //il nome della persona da contattare e la data.

            /*var country = "USA";
            var region = "WA";
            var query = from c in db.Customers
                        where c.Country == country && c.Region == region
                        join o in db.Orders on c.CustomerID equals o.CustomerID
                        orderby o.OrderDate descending
                        select new { c.ContactName, o.OrderDate };

            string queryDefinition = "for customers in Washington " +
                                     "show contact name and order date" +
                                     " order the results by order date in descending order";
            showResults(query, queryDefinition, query.ToString(), false, "query 5");*/
        }

        private void Statistiche(String target)
        {
            // definizione della query:
            //Per i clienti che hanno fatto ordini
            //ordinati per nome,
            //visualizzare l'importo totale ordinato
            /*var orderList = from o in db.Orders
                            join c in db.Customers on o.CustomerID equals c.CustomerID
                            orderby c.ContactName
                            select new
                            {
                                Name = c.ContactName,
                                c.Country,
                                c.City,
                                Revenue = o.Order_Details.Sum(p => p.UnitPrice * p.Quantity)
                            };

            var customerRevenue = from o in orderList
                                  group o by o.Name into g
                                  select new
                                  {
                                      CustomerName = g.Key,
                                      Country = g.Select(a => a.Country).First(),
                                      City = g.Select(b => b.City).First(),
                                      TotalRevenue = g.Sum(o => o.Revenue)
                                  };

            string queryDefinition = "show the total revenue for each customer" +
                                     " which issued orders" +
                                     " order the results by name of the customer";
            showResults(customerRevenue, queryDefinition, customerRevenue.ToString(), false, "query 6");*/
        }
        private void UserSideInserimentoAttivita()
        { 
            // definizione della query:
            // Visualizzare contatto e data ordine dei clienti di Londra che hanno fatto ordini
            // ordinando il risultato per contatto cliente
            /*var query = from c in db.Customers
                    from o in c.Orders
                    where c.City == "London"
                    orderby c.ContactName
                    select new { c.ContactName, o.OrderDate };

            string queryDefinition = "show the contact name and order date for London customers which issued orders; show the results ordered by contact name";
            showResults(query, queryDefinition, query.ToString(), false, "query 7");*/
        }
        private void VisioneAttivita()
        {
            // definizione della query:
            // Visualizzare i clienti che fanno tutti gli ordini con consegna nella città dove risiedono
            /*var query = from c in db.Customers
                    where c.Orders.All(o => o.ShipCity == c.City)
                    select c;

            string queryDefinition = "show customers which issued orders and ship in the customer city ";
            showResults(query, queryDefinition, query.ToString(), false, "query 8");*/
        }

        private void InserimentoObbiettivi()
        {
            // definizione della query:
            // Visualizzare, per ciascuna categoria a cui appartengono più di 10 prodotti,
            // il prezzo medio dei prodotti che vi appartengono;
            /*var query = from p in db.Products
                                where p.Category.Products.Count() > 10
                                group p by new
                                {
                                    p.Category.CategoryID,
                                    p.Category.CategoryName
                                } into g
                                select new
                                {
                                    g.Key.CategoryID,
                                    g.Key.CategoryName,
                                    AvgPrice = g.Average(p => p.UnitPrice)
                                };

            string queryDefinition = "for each category, with more than 10 products, show the average price of products belonging to that category";
            showResults(query, queryDefinition, query.ToString(), false,"query 9");*/
        }
        private void VisoneObbiettivi()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void VisualizzazioneCalendario()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void VisioneAchievemtnPercentuale()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void VisioneAchievemtnSingoliPercentuale()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void DisplaySoddisfazione()//TODO a  fine giornata
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void ModificaBudget()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void InserimetnoAttivitaOra()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/

        }

        private void InserimentoCompitoTransazione()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void InserimetnoCompitoCompletamento()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void CompletamentoAttivitaOra() //dentro chiama update percentuali
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void CalcoloPercentuale()//riceve una enume
        {

        }

        private void ApplicazionePenitenza()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

        private void AggiuntaUtente()
        {
            // definizione della query:
            // dammi tutti i prodotti che sono sotto il livello di scorta e non sono stati dismessi
            // ordinati per codice fornitore
            /*var query =
               from p in db.Products
               where p.UnitsInStock <= p.ReorderLevel && !p.Discontinued
               orderby p.SupplierID ascending
               select p;
            string queryDefinition = "filter for Products that have stock below their reorder level and are not discontinued, order by SupplierID";
            showResults(query, queryDefinition, query.ToString(), true,"query 10");*/
        }

    }

    #endregion private methods

}


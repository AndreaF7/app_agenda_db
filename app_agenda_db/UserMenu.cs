﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace app_agenda_db
{
    public partial class UserMenu : Form
    {
        private List<Panel> leftPanelComponents;
        private int utente;
        DateTime dt = DateTime.Today;
        Calendar ca = CultureInfo.InvariantCulture.Calendar;
        private string nomeOriginale = null;
        private string descrizioneObiettivoOriginale = null;
        private readonly Color[] colors = {Color.Blue, Color.Violet, Color.Red, Color.Yellow};
        private Periodo periodo = Periodo.Mese;
        private TipoDato tipoDato = TipoDato.Attivita;

        public UserMenu(int idUtente) //Aggiungere string utente(id utente)
        {
            leftPanelComponents = new List<Panel>();
            InitializeComponent();
            this.leftPanelComponents.Add(this.obiettivo_pannello_iniziale);
            this.leftPanelComponents.Add(this.pannelloCalendario);
            this.leftPanelComponents.Add(this.pannelloAttivita);
            this.leftPanelComponents.Add(this.pannelloStatistiche);
            this.leftPanelComponents.Add(this.calendarioGiornalieroTableLayoutPanel);
            this.leftPanelComponents.Add(this.pannelloBudget);
            this.leftPanelComponents.Add(this.pannelloAchievement);
            this.leftPanelComponents.ForEach(p => p.Hide());
            this.RiempiCalendarioMensile(dt);
            this.calendarioGiornalieroTableLayoutPanel.Show();
            this.utente = idUtente;//utente
            //aggiunta colori a combo box colori, ora test, poi colori
            this.attivita_comboColore.DrawMode = DrawMode.OwnerDrawFixed;
            this.attivita_comboColore.Items.Clear();
            foreach (Color color in colors) this.attivita_comboColore.Items.Add(color);
            attivita_comboColore.DrawItem += attivita_comboColore_DrawItemColore;
        }

        //---------------utility functions-------------

        private void RiempiCalendarioMensile(DateTime d)
        {
            DateTime first = new DateTime(d.Year, d.Month, 1);
            int i = ca.GetDaysInMonth(dt.Year, dt.Month);
            int n = 35;
            this.calendarioMeseTextBox.Text = ScegliMese(d);
            //Update with correct days
            foreach (Control c in this.calendarioMensileTableLayoutPanel.Controls)
            {
                if (c is Button)
                {
                    if (n == i)
                    {
                        ((Button)c).Text = i.ToString();
                        ((Button)c).Enabled = true;
                        i--;
                    }
                    else
                    {
                        ((Button)c).Text = "";
                        ((Button)c).Enabled = false;
                    }
                    n--;
                    ((Button)c).Click += new EventHandler(this.BottoneGiornaliero_Click);
                }
            }

            this.calendarioMeseGiorno1.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno2.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno3.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno4.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno5.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno6.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.calendarioMeseGiorno7.Text = this.ScegliGiorno(first);
            first = first.AddDays(1);
            this.pannelloCalendario.Refresh();
        }

        private string ScegliMese(DateTime d)
        {
            switch(d.Month)
            {
                case 1:
                    return "Gennaio";
                case 2:
                    return "Febbraio";
                case 3:
                    return "Marzo";
                case 4:
                    return "Aprile";
                case 5:
                    return "Maggio";
                case 6:
                    return "Giugno";
                case 7:
                    return "Luglio";
                case 8:
                    return "Agosto";
                case 9:
                    return "Settembre";
                case 10:
                    return "Ottobre";
                case 11:
                    return "Novembre";
                default:
                    return "Dicembre";
            }
        }

        private string ScegliGiorno(DateTime d)
        {
            switch(d.DayOfWeek.ToString())
            {
                case "Monday":
                    return "Lun";
                case "Tuesday":
                    return "Mar";
                case "Wednesday":
                    return "Mer";
                case "Thursday":
                    return "Gio";
                case "Friday":
                    return "Ven";
                case "Saturday":
                    return "Sab";
                default:
                    return "Dom";
            }
        }

        private void attivita_comboColore_DrawItemColore(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            // Clear the background appropriately.
            e.DrawBackground();
            int MarginHeight = 4;
            int MarginWidth = 4;
            // Draw the color sample.
            int hgt = e.Bounds.Height - 2 * MarginHeight;
            Rectangle rect = new Rectangle(
                e.Bounds.X + MarginWidth,
                e.Bounds.Y + MarginHeight,
                hgt, hgt);
            ComboBox cbo = sender as ComboBox;
            Color color = (Color)cbo.Items[e.Index];
            using (SolidBrush brush = new SolidBrush(color))
            {
                e.Graphics.FillRectangle(brush, rect);
            }

            // Outline the sample in black.
            e.Graphics.DrawRectangle(Pens.Black, rect);

            // Draw the color's name to the right.
            using (Font font = new Font(cbo.Font.FontFamily,
                cbo.Font.Size * 0.75f, FontStyle.Bold))
            {
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;
                    int x = hgt + 2 * MarginWidth;
                    int y = e.Bounds.Y + e.Bounds.Height / 2;
                    e.Graphics.TextRenderingHint =
                        System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                    e.Graphics.DrawString(color.Name, font,
                        Brushes.Black, x, y, sf);
                }
            }
            e.DrawFocusRectangle();
        }

        //---------------utility functions-------------
        #region utility functions

        //takes in input a control and resets the text of all the contained items in it
        private void ResetTextIteratively(Control container)
        {
            foreach (Control c in container.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).ResetText();
                if (c.Controls.Count > 0)
                    ResetTextIteratively(c);
            }
        }

        private void EnableAllButtons(Control container)
        {
            foreach (Control el in container.Controls)
            {
                if (el is Button)
                    ((Button)el).Enabled = true;
            }
        }

        #endregion




        //----------------------------------------------------------------------------

        //----------------------left panel-------------------------
        #region left panel

        private void B_Calendario_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.calendarioGiornalieroTableLayoutPanel.Show();
        }


        private void B_Obiettivi_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.obiettivo_pannello_iniziale.Show();
        }

        private void B_Achievement_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.RiempiListaAchievement();
            this.pannelloAchievement.Show();
        }

        private void RiempiListaAchievement()
        {
            this.achievementListBox.Items.Clear();
            this.achievementListBox.Items.AddRange(Queries.GetAchievement().Select(a => a.nome).ToArray());
        }

        private void B_Budget_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.pannelloBudget.Show();
        }

        #endregion

        //------------pannello statistiche----------------------
        #region pannello statistiche

        private void B_Statistiche_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.pannelloStatistiche.Show();
            this.periodo = Periodo.Settimana;
            this.tipoDato = TipoDato.Attivita;
            this.statistiche_dateTimePicker.Value = DateTime.Today.AddDays(-3);
            Statistiche_Reset();
        }

        private void Statistiche_Reset()
        {
            EnableAllButtons(this.statisticheSplitContainer.Panel2);
            EnableAllButtons(this.splitContainer2.Panel2);
            DateTime frm = this.statistiche_dateTimePicker.Value;
            DateTime to = frm.Date;

            switch (this.periodo)
            {
                case Periodo.Settimana://TODO modify da inizio a fine settimana
                    this.statistiche_bSettimana.Enabled = false;
                    to = frm.AddDays(7);
                    break;
                case Periodo.Mese: //da inizio a fine mese
                    this.statistiche_bMese.Enabled = false;
                    to = frm.AddMonths(1);
                    break;
                case Periodo.Anno: // da inizio a fine anno
                    this.statistiche_bAnno.Enabled = false;
                    to = frm.AddYears(1);
                    break;
                default:
                    throw new ArgumentException();
            }
            var toPlot = from att in Queries.db.ATTIVITÀs
                         join aOra in Queries.getAttivitaUtentePeriodo(this.utente, frm, to) on att.id equals aOra.attività
                         select new {ATTIVITÀ_ORA = aOra, ATTIVITÀ =  att};

            //filtra in base alla tipologia
            this.graficoStatistiche.Series["plot"].Points.Clear();
            //this.chartArea1.AxisX.Title = "Giorni";
            switch (this.tipoDato)
            {
                case TipoDato.Spesa://TODO aggiungere barretta budget
                    this.statistiche_bSpese.Enabled = false;
                    //this.chartArea1.AxisY.Title = "Numero Attività Completate";
                    var saldoEuro = Queries.db.SALDOs.Where(s => s.valuta == Queries.VALUTA_EURO && s.utente == this.utente);
                    foreach (int i in Enumerable.Range(1, (int)(to - frm).TotalDays))//range giorni, x è giorno, y è count where giorno uguale giorno
                    {
                        var xDate = frm.AddDays(i);
                        var temp = saldoEuro.Where(t => (t.data_ora.Year > xDate.Year ? 1 : t.data_ora.Year < xDate.Year ? -1 : t.data_ora.Month > xDate.Month ? 1 : t.data_ora.Month < xDate.Month ? -1 : t.data_ora.Day > xDate.Day ? 1 : t.data_ora.Day < xDate.Day ? -1 : 0) == 0);
                        if (temp.Count() > 0)
                            this.graficoStatistiche.Series["plot"].Points.AddXY(xDate, temp.OrderByDescending(s => s.data_ora).First().quantità);
                    }
                    break;
                case TipoDato.Attivita:
                    this.statistiche_bAttivita.Enabled = false;
                    //this.chartArea1.AxisY.Title = "Numero Attività Completate";
                    foreach (int i in Enumerable.Range(1,(int)(to - frm).TotalDays))//range giorni, x è giorno, y è count where giorno uguale giorno
                    {
                        var xDate = frm.AddDays(i);
                        this.graficoStatistiche.Series["plot"].Points.AddXY(xDate, toPlot.Where(t => (t.ATTIVITÀ_ORA.giorno.Year > xDate.Year ? 1 : t.ATTIVITÀ_ORA.giorno.Year < xDate.Year ? -1 : t.ATTIVITÀ_ORA.giorno.Month > xDate.Month ? 1 : t.ATTIVITÀ_ORA.giorno.Month < xDate.Month ? -1 : t.ATTIVITÀ_ORA.giorno.Day > xDate.Day ? 1 : t.ATTIVITÀ_ORA.giorno.Day < xDate.Day ? -1 : 0) == 0).Count());
                    }
                    break;
                case TipoDato.PP:
                    this.statistiche_bPP.Enabled = false;
                    //this.chartArea1.AxisY.Title = "Punti Produttivià";
                    var saldoPP = Queries.db.SALDOs.Where(s => s.valuta == Queries.VALUTA_PP && s.utente == this.utente);
                    foreach (int i in Enumerable.Range(1,(int)(to - frm).TotalDays))//range giorni, x è giorno, y è count where giorno uguale giorno
                    {
                        var xDate = frm.AddDays(i);
                        var temp = saldoPP.Where(t => (t.data_ora.Year > xDate.Year ? 1 : t.data_ora.Year < xDate.Year ? -1 : t.data_ora.Month > xDate.Month ? 1 : t.data_ora.Month < xDate.Month ? -1 : t.data_ora.Day > xDate.Day ? 1 : t.data_ora.Day < xDate.Day ? -1 : 0) == 0);
                        if(temp.Count() > 0)
                            this.graficoStatistiche.Series["plot"].Points.AddXY(xDate, temp.OrderByDescending(s => s.data_ora).First().quantità);
                    }
                    break;
                case TipoDato.PV://TODO aggiungere barretta giornaliero
                    this.statistiche_bPV.Enabled = false;
                    //this.chartArea1.AxisY.Title = "Numero Attività Completate";
                    var saldoPV = Queries.db.SALDOs.Where(s => s.valuta == Queries.VALUTA_PV && s.utente == this.utente);
                    foreach (int i in Enumerable.Range(1, (int)(to - frm).TotalDays))//range giorni, x è giorno, y è count where giorno uguale giorno
                    {
                        var xDate = frm.AddDays(i);
                        var temp = saldoPV.Where(t => (t.data_ora.Year > xDate.Year ? 1 : t.data_ora.Year < xDate.Year ? -1 : t.data_ora.Month > xDate.Month ? 1 : t.data_ora.Month < xDate.Month ? -1 : t.data_ora.Day > xDate.Day ? 1 : t.data_ora.Day < xDate.Day ? -1 : 0) == 0);
                        if (temp.Count() > 0)
                            this.graficoStatistiche.Series["plot"].Points.AddXY(xDate, temp.OrderByDescending(s => s.data_ora).First().quantità);
                    }
                    break;
                default:
                    throw new ArgumentException();
            }
        }

        private void Statistiche_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            Statistiche_Reset();
        }

        private void Statistiche_bAttivita_Click(object sender, EventArgs e)
        {
            this.tipoDato = TipoDato.Attivita;
            Statistiche_Reset();
        }


        private void Statistiche_bSpese_Click(object sender, EventArgs e)
        {
            this.tipoDato = TipoDato.Spesa;
            Statistiche_Reset();
        }

        private void Statistiche_bPP_Click(object sender, EventArgs e)
        {
            this.tipoDato = TipoDato.PP;
            Statistiche_Reset();
        }

        private void Statistiche_bPV_Click(object sender, EventArgs e)
        {
            this.tipoDato = TipoDato.PV;
            Statistiche_Reset();
        }

        private void Statistiche_bSettimana_Click(object sender, EventArgs e)
        {
            this.periodo = Periodo.Settimana;
            Statistiche_Reset();
        }

        private void Statistiche_bMese_Click(object sender, EventArgs e)
        {
            this.periodo = Periodo.Mese;
            Statistiche_Reset();
        }

        private void Statistiche_bAnno_Click(object sender, EventArgs e)
        {
            this.periodo = Periodo.Anno;
            Statistiche_Reset();
        }

        #endregion

        //------------pannello attivita----------------------
        #region pannello attività

        private void Attivita_Reset()
        {
            this.attivita_bElimina.Enabled = false;
            this.attivita_bSalva.Enabled = false;
            this.attivita_bAggiungi.Enabled = false;
            this.attivita_bAvvia.Enabled = false;
            this.attivitaADateTimePicker.Enabled = false;
            this.attivitaDaDateTimePicker.Enabled = false;
            foreach (Control i in this.pannelloAttivita.Controls)
            {
                i.ResetText();
            }
            Attivita_rigeneraComboAttività();
            Attivita_rigeneraComboCategoria();
            ResetTextIteratively(this.pannelloAttivita);
            this.attivita_comboColore.SelectedIndex = -1;
            this.nomeOriginale = null;

        }

        //rigenera combo box con query
        private void Attivita_rigeneraComboAttività()
        {
            var attivita = Queries.GetAttivitaUtente(this.utente);
            this.attivita_comboSelezionaAttivita.Items.Clear();
            this.attivita_comboSelezionaAttivita.Items.AddRange(attivita.Select(a => a.nome).ToArray());
            this.attivita_comboSelezionaAttivita.ResetText();
        }

        private void Attivita_rigeneraComboCategoria()
        {
            this.attivita_comboCategoria.Items.Clear();
            this.attivita_comboCategoria.Items.AddRange(Queries.db.TIPOLOGIAs.Select(t => t.nome).ToArray());
            this.attivita_comboCategoria.ResetText();
            this.attivita_textAggiungiCategoria.ResetText();
        }


        private void B_Attivita_Click(object sender, EventArgs e)
        {
            this.leftPanelComponents.ForEach(c => c.Hide());
            this.pannelloAttivita.Show();
            Attivita_Reset();
        }

        private void Attivita_bAggiungi_Click(object sender, EventArgs e)
        {
            Attivita_Reset();
        }

        private void Attivita_bSalva_Click(object sender, EventArgs e)
        {
            //query per salvare modifiche
            var costi = new Dictionary<string, int>();
            var profitti = new Dictionary<string, int>();
            if (this.attivita_textCostoEuro.Text.Length > 0)
                costi.Add(Queries.VALUTA_EURO, Int32.Parse(attivita_textCostoEuro.Text));
            if (this.attivita_textCostoPV.Text.Length > 0)
                costi.Add(Queries.VALUTA_PV, Int32.Parse(attivita_textCostoPV.Text));
            if (this.attivita_textPremioEuro.Text.Length > 0)
                profitti.Add(Queries.VALUTA_EURO, Int32.Parse(attivita_textPremioEuro.Text));
            if (this.attivita_textPremioPV.Text.Length > 0)
                profitti.Add(Queries.VALUTA_PV, Int32.Parse(attivita_textPremioPV.Text));
            if (this.attivita_textPremioPP.Text.Length > 0)
                profitti.Add(Queries.VALUTA_PP, Int32.Parse(attivita_textPremioPP.Text));
            Queries.SetAttivitaUtente(this.utente, this.nomeOriginale, this.attivita_textNome.Text, this.attivita_comboColore.SelectedIndex, this.attivita_textDescrizione.Text, this.attivita_comboCategoria.SelectedItem.ToString(), costi, profitti, this.attivita_comboPenitenza.SelectedIndex);

            Attivita_Reset();
        }

        private void Attivita_bElimina_Click(object sender, EventArgs e)
        {
            Queries.EliminaAttivitaUtentePerNome(this.utente, this.nomeOriginale);
            Attivita_Reset();
        }

        private void Attivita_ComboBoxListaAttivita_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selezione = (string)this.attivita_comboSelezionaAttivita.SelectedItem;
            Attivita_Reset();
            this.attivita_bElimina.Enabled = true;
            this.attivita_bAggiungi.Enabled = true;
            this.attivita_bAvvia.Enabled = true;
            this.attivitaDaDateTimePicker.Enabled = true;
            this.attivitaADateTimePicker.Enabled = true;
            Attivita_FillConInfoAttivita(Queries.GetAttivitaUtentePerNome(this.utente, selezione));
            this.nomeOriginale = selezione;
        }

        private void Attivita_FillConInfoAttivita(ATTIVITÀ attivita)
        {
            this.attivita_textNome.Text = attivita.nome;
            this.attivita_comboCategoria.Text = Queries.db.CARATTERIZZAZIONEs.Where(t => t.attività == attivita.id).First().tipologia;
            this.attivita_textDescrizione.Text = attivita.descrizione;
            this.attivita_comboColore.SelectedIndex = Decimal.ToInt32(attivita.colore);
            var costEur = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.attivita_textCostoEuro.Text = costEur is null ? "" : costEur.quantità.ToString();
            var costPV = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.attivita_textCostoPV.Text = costPV is null ? "" : costPV.quantità.ToString();
            var profEur = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.attivita_textPremioEuro.Text = profEur is null ? "" : profEur.quantità.ToString();
            var profPV = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.attivita_textPremioPV.Text = profPV is null ? "" : profPV.quantità.ToString();
            var profPP = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PP).FirstOrDefault();
            this.attivita_textPremioPP.Text = profPP is null ? "" : profPP.quantità.ToString();
            var penitenza = Queries.GetPenitenzaAttivitaUtentePerNome(this.utente, attivita.nome);
            this.attivita_textPenitenza.Text = penitenza is null ? "" : (penitenza.descrizione is null ? "" : penitenza.descrizione);
        }

        private void Attivita_bAvvia_Click(object sender, EventArgs e)
        {
            if (this.attivitaADateTimePicker.Value.DayOfYear > this.attivitaDaDateTimePicker.Value.DayOfYear)
            {
                Queries.SetAttivitaOraUtente(this.utente, this.attivita_textNome.Text, this.attivitaDaDateTimePicker.Value, 
                    new TimeSpan[] { this.attivitaDaTimePicker.Value.TimeOfDay, this.attivitaADateTimePicker.Value.TimeOfDay});
                this.Attivita_Reset();
            }
            else
            {
                MessageBox.Show("Valori errati!");
            }
        }

        private void Attivita_textNome_TextChanged(object sender, EventArgs e)
        {
            Attivita_TriggerSalva();
        }

        private void TextDescrizione_TextChanged(object sender, EventArgs e)
        {
            Attivita_TriggerSalva();
        }

        private void Attivita_comboColore_SelectedIndexChanged(object sender, EventArgs e)
        {
            Attivita_TriggerSalva();
        }

        private void Attivita_comboCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            Attivita_TriggerSalva();
        }

        private void Attivita_TriggerSalva()
        {
            this.attivita_bSalva.Enabled = this.attivita_textNome.Text.Length > 0 &&
                this.attivita_comboColore.SelectedIndex >= 0 && this.attivita_textDescrizione.Text.Length > 0
                && this.attivita_comboCategoria.SelectedIndex >= 0;
        }

        #endregion

        //-------------------pannello obiettivi----------------------------------
        #region pannello obiettivi

        bool aggiungiAttivitaClick = false;
        bool aggiungiObiettivoClick = false;
        bool aggiungiObiettivoCanBeEnabled = true;
        bool aggiungiAttivitaCanBeEnabled = true;
        bool guardaObiettiviCompletamento = false;

        private void ControlloAttivareSalva()
        {
            //controllare se tutti i campi da inserire in un nuovo obiettivo
            //sono stati inseriti: se si, attiva il bottone salva
            if (this.obiettivoDescrizioneTextBox.Text.Length > 0 && 
                this.obiettivoNVolteTextBox.Text.Length > 0 && this.obiettivoSceltaAttivitaComboBox.SelectedIndex > -1 &&
                this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex > -1 &&
                this.aggiungiAttivitaClick && this.aggiungiObiettivoClick)
            {
                this.obiettivoSalvaButton.Enabled = true;
            }
            else
            {
                this.obiettivoSalvaButton.Enabled = false;
            }
        }

        private void ControlloAttivareAggiungiAttivita()
        {
            if (this.obiettivoSceltaAttivitaComboBox.SelectedIndex > -1 && this.obiettivoNVolteTextBox.Text.Length > 0 &&
                this.aggiungiAttivitaCanBeEnabled == true)
            {
                this.obiettivoAggiungiAttivitaButton.Enabled = true;
            }
            else
            {
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
        }

        private void ControlloAttivareAggiungiObiettivo()
        {
            if (this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex > -1 && this.aggiungiObiettivoCanBeEnabled == true)
            {
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = true;
            }
            else
            {
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            }
        }

        private void ObiettivoDescrizioneTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalva();
        }

        private void ObiettivoNVolteTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalva();
            this.ControlloAttivareAggiungiAttivita();
        }

        private void ObiettivoSceltaSottoObiettivoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareAggiungiObiettivo();
            this.ControlloAttivareSalva();
        }

        private void ObiettivoSceltaAttivitaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareAggiungiAttivita();
            this.ControlloAttivareSalva();
        }

        private void ObiettivoAggiungiAttivitaButton_Click(object sender, EventArgs e)
        {
            this.aggiungiAttivitaClick = true;
            this.ControlloAttivareSalva();
        }

        private void ObiettivoAggiungiSottoObiettivoButton_Click(object sender, EventArgs e)
        {
            this.aggiungiObiettivoClick = true;
            this.ControlloAttivareSalva();
        }

        private void ResetEachText()
        {
            this.obiettivoDescrizioneTextBox.ResetText();
            this.obiettivoNVolteTextBox.ResetText();
            this.obiettivoSceltaSottoObiettivoComboBox.Items.Clear();
            this.obiettivoSceltaSottoObiettivoComboBox.ResetText();
            this.obiettivoSceltaSottoObiettivoComboBox.Items.Add("Sotto obiettivo");
            this.obiettivoSceltaAttivitaComboBox.Items.Clear();
            this.obiettivoSceltaAttivitaComboBox.ResetText();
            this.obiettivoSceltaAttivitaComboBox.Items.Add("Sotto obiettivo");
            this.obiettivoSceltaAttivitaComboBox.SelectedIndex = -1;
            this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndex = -1;
            this.obiettiviLista.SelectedIndex = -1;
            this.obiettivoTermineDurataDateTimePicker.ResetText();
            this.obiettivoInizioDurataDateTimePicker.ResetText();
            this.AggiornaListaObiettivi();
        }

        private void AggiornaListaObiettivi()
        {
            var obiettivi = Queries.GetObiettiviUtente(this.utente);
            this.obiettiviLista.Items.Clear();
            this.obiettiviLista.Items.AddRange(obiettivi.Select(o => o.descrizione).ToArray());
            this.obiettiviLista.ResetText();
        }

        private void ObiettivoAggiungiButton_Click(object sender, EventArgs e)
        {
            this.obiettivoAggiungiButton.Enabled = false;
            this.ResetEachText();
            this.obiettivoDescrizioneTextBox.Enabled = true;
            this.obiettivoNVolteTextBox.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
        }

        private void ObiettiviLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selezione = (string)this.obiettiviLista.SelectedItem;
            this.ResetEachText();
            this.descrizioneObiettivoOriginale = selezione;
            var obiettivo = Queries.GetObiettivoUtentePerNome(this.utente, selezione);
            this.obiettivoDescrizioneTextBox.Text = obiettivo.descrizione;
            this.obiettivoSceltaAttivitaComboBox.Items.AddRange(Queries.GetAttivitaUtente(this.utente).Select(a => a.nome).ToArray());
            this.obiettivoNVolteTextBox.Text = Queries.GetNVolteAttivitaPerObiettivoUtente(this.utente, this.obiettivoSceltaAttivitaComboBox.Items[0].ToString(), selezione).ToString();

            /*this.attivita_comboCategoria.Text = Queries.db.CARATTERIZZAZIONEs.Where(t => t.attività == attivita.id).First().tipologia;
            this.attivita_textDescrizione.Text = attivita.descrizione;
            this.attivita_comboColore.SelectedIndex = Decimal.ToInt32(attivita.colore);
            var costEur = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.attivita_textCostoEuro.Text = costEur is null ? "" : costEur.quantità.ToString();
            var costPV = Queries.db.COSTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.attivita_textCostoPV.Text = costPV is null ? "" : costPV.quantità.ToString();
            var profEur = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_EURO).FirstOrDefault();
            this.attivita_textPremioEuro.Text = profEur is null ? "" : profEur.quantità.ToString();
            var profPV = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PV).FirstOrDefault();
            this.attivita_textPremioPV.Text = profPV is null ? "" : profPV.quantità.ToString();
            var profPP = Queries.db.PROFITTOs.Where(t => t.attività == attivita.id && t.valuta == Queries.VALUTA_PP).FirstOrDefault();
            this.attivita_textPremioPP.Text = profPP is null ? "" : profPP.quantità.ToString();
            var penitenza = Queries.GetPenitenzaAttivitaUtentePerNome(this.utente, attivita.nome);*/

            this.obiettivoAggiungiButton.Enabled = true;
            this.obiettivoSalvaButton.Enabled = true;
            this.aggiungiAttivitaClick = false;
            this.aggiungiObiettivoClick = false;
            this.obiettivoEliminaButton.Enabled = true;
            this.obiettivoInizioDurataDateTimePicker.Enabled = true;
            this.obiettivoTermineDurataDateTimePicker.Enabled = true;
            this.obiettivoAvviaButton.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
        }

        private void ObiettivoEliminaButton_Click(object sender, EventArgs e)
        {
            this.obiettivoAggiungiButton.Enabled = false;
            this.obiettiviLista.Items.Remove(this.obiettivoDescrizioneTextBox.Text);
            this.ResetEachText();
            this.obiettivoDescrizioneTextBox.Enabled = true;
            this.obiettivoNVolteTextBox.Enabled = true;
            this.aggiungiObiettivoCanBeEnabled = true;
            this.aggiungiAttivitaCanBeEnabled = true;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
        }

        private void ObiettivoSalvaButton_Click(object sender, EventArgs e)
        {
            int n;
            if (int.TryParse(this.obiettivoNVolteTextBox.Text, out n))
            {
                this.obiettivoAggiungiButton.Enabled = false;
                this.obiettiviLista.Items.Add(this.obiettivoDescrizioneTextBox.Text);
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.aggiungiAttivitaCanBeEnabled = true;
                this.obiettivoEliminaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("N volte deve essere un intero!");
            }
        }

        private void ObiettivoAvviaButton_Click(object sender, EventArgs e)
        {
            if (this.obiettivoTermineDurataDateTimePicker.Value.DayOfYear > this.obiettivoInizioDurataDateTimePicker.Value.DayOfYear)
            {
                this.obiettivoAggiungiButton.Enabled = false;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.aggiungiAttivitaCanBeEnabled = true;
                this.obiettivoEliminaButton.Enabled = false;
                this.obiettivoSalvaButton.Enabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("Valori errati");
            }
        }

        private void ObiettivoVediButton_Click(object sender, EventArgs e)
        {
            if (!this.guardaObiettiviCompletamento)
            {
                OBIETTIVO ob = new OBIETTIVO
                {
                    descrizione = "Da query"
                };
                OBBIETTIVO_COMPLETAMENTO obc = new OBBIETTIVO_COMPLETAMENTO
                {
                    
                };
                this.guardaObiettiviCompletamento = true;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = false;
                this.obiettivoDescrizioneTextBox.Text = ob.descrizione;
                this.obiettivoNVolteTextBox.Enabled = false;
                this.aggiungiObiettivoCanBeEnabled = false;
                this.obiettivoInizioDurataDateTimePicker.Enabled = false;
                this.obiettivoTermineDurataDateTimePicker.Enabled = false;
                this.obiettivoAvviaButton.Enabled = false;
                this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
                this.obiettivoAggiungiAttivitaButton.Enabled = false;
            }
            else
            {
                this.guardaObiettiviCompletamento = false;
                this.ResetEachText();
                this.obiettivoDescrizioneTextBox.Enabled = true;
                this.obiettivoNVolteTextBox.Enabled = true;
                this.aggiungiObiettivoCanBeEnabled = true;
                this.obiettivoInizioDurataDateTimePicker.Enabled = true;
                this.obiettivoTermineDurataDateTimePicker.Enabled = true;
                this.obiettivoAvviaButton.Enabled = true;
            }
        }

        #endregion

        //-------------------pannello calendario----------------------------------
        #region pannello calendario

        private void CalendarioMeseButton_Click(object sender, EventArgs e)
        {
            leftPanelComponents.ForEach(p => p.Hide());
            this.pannelloCalendario.Show();
            this.RiempiAttivita();
        }

        private void BottoneGiornaliero_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            int n = Int32.Parse(b.Text);
            DateTime old = dt;
            dt = new DateTime(ca.GetYear(old), ca.GetMonth(old), 1);
            dt = ca.AddDays(dt, n-1);
            this.calendarioGiornalieroDateTimePicker.Value = dt.Date;
            leftPanelComponents.ForEach(p => p.Hide());
            this.calendarioGiornalieroTableLayoutPanel.Show();
            this.RiempiAttivita();
        }

        private void RiempiAttivita()
        {
            this.calendarioAttivitaListBox.Items.Clear();
            var attGiorno = from a in Queries.db.ATTIVITÀs
                            join ag in Queries.getAttivitaUtentePeriodo(this.utente, this.calendarioGiornalieroDateTimePicker.Value, this.calendarioGiornalieroDateTimePicker.Value) on a.id equals ag.attività
                            orderby ag.ora_inizio
                            select a.nome;
            this.calendarioAttivitaListBox.Items.AddRange(attGiorno.ToArray());
        }

        private void CalendarioMeseFrecciaDestra_Click(object sender, EventArgs e)
        {
            dt = ca.AddMonths(dt, 1);
            this.RiempiCalendarioMensile(dt);
        }

        private void CalendarioMeseBottoneSinistra_Click(object sender, EventArgs e)
        {
            dt = ca.AddMonths(dt,-1);
            this.RiempiCalendarioMensile(dt);
        }

        private void CalendarioGiornalieroDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            dt = this.calendarioGiornalieroDateTimePicker.Value;
            this.RiempiCalendarioMensile(dt);
            this.RiempiAttivita();
        }

        private void CalendarioAttivitaCheckedListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.leftPanelComponents.ForEach(p => p.Hide());
/*<<<<<<< HEAD
            this.attivita_comboSelezionaAttivita.SelectedItem = this.CalendarioAttivitaCheckedListBox.SelectedItem;
            this.pannelloAttivita.Show();
=======*/
            this.B_Attivita_Click(sender, e);
            this.attivita_bElimina.Enabled = true;
            this.attivita_bAggiungi.Enabled = true;
            this.attivita_bAvvia.Enabled = true;
            this.attivitaDaDateTimePicker.Enabled = true;
            this.attivitaADateTimePicker.Enabled = true;
            string selezione = (string)this.calendarioAttivitaListBox.SelectedItem;
            Attivita_FillConInfoAttivita(Queries.GetAttivitaUtentePerNome(this.utente, selezione));
            this.nomeOriginale = selezione;
        }

        private void CalendarioAttivitaListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var nomeAttivita = (string)sender;
            var att = (from ag in Queries.getAttivitaUtentePeriodo(this.utente, this.calendarioGiornalieroDateTimePicker.Value, this.calendarioGiornalieroDateTimePicker.Value)
                       orderby ag.ora_inizio
                       select ag).First();
            Queries.CambiaSelezioneCompletamentoAttivita(att);
//>>>>>>> c806dd47982216ae1021c31b78fccd90c25aa539
        }

        #endregion

        #region budget

        private void ControlloAttivareSalvaBudget()
        {
            if (this.budgetDescrizioneSpesaTextBox.Text.Length > 0 && this.budgetSpesaAccontoTextBox.Text.Length > 0)
            {
                this.budgetSalvaSpesaButton.Enabled = true;
            }
            else
            {
                this.budgetSalvaSpesaButton.Enabled = false;
            }
        }

        private void BudgetDescrizioneSpesaTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalvaBudget();
        }

        private void BudgetSpesaAccontoTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ControlloAttivareSalvaBudget();
        }

        private void BudgetImpostaButton_Click(object sender, EventArgs e)
        {
            Queries.SetBudgetUtente(this.utente, Int32.Parse(this.budgetBudgetTextBox.Text), DateTime.Today);
            ResettaTestoBudget();
        }

        private void BudgetSpesePeriodicheComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*ATTIVITÀ nuovaAttivita = new ATTIVITÀ
            {
                id = Queries.GetAttivitaUtentePerNome(this.utente, this.attivita_comboSelezionaAttivita.Text).id, //to retrieve from list Attività
                nome = this.attivita_textNome.Text,
                colore = this.attivita_comboColore.SelectedIndex,
                descrizione = this.attivita_textDescrizione.Text
            };*/
            //query per salvare modifiche TODO modifica secondo nuova query
            //Queries.SetAttivitaUtente(this.utente, nuovaAttivita, "generico", this.attivita_comboPenitenza.SelectedIndex);
            string desc = this.budgetSpesePeriodicheComboBox.GetItemText(this.budgetSpesePeriodicheComboBox.SelectedItem);
            this.ResettaTestoBudget();
            this.budgetDescrizioneSpesaTextBox.Text = desc;
            this.budgetAggiungiSpesaButton.Enabled = true;
            this.budgetEliminaSpesaButton.Enabled = true;
        }

        private void ResettaTestoBudget()
        {
            this.budgetBudgetTextBox.ResetText();
            this.budgetDescrizioneSpesaTextBox.ResetText();
            this.budgetSpesaAccontoTextBox.ResetText();
            this.budgetSpesePeriodicheComboBox.SelectedIndex = -1;
            this.budgetAggiungiSpesaButton.Enabled = false;
            this.budgetEliminaSpesaButton.Enabled = false;
        }

        private void BudgetAggiungiSpesaButton_Click(object sender, EventArgs e)
        {
            this.ResettaTestoBudget();
        }

        private void BudgetEliminaSpesaButton_Click(object sender, EventArgs e)
        {
            this.budgetSpesePeriodicheComboBox.Items.Remove(this.budgetDescrizioneSpesaTextBox.Text);
            this.ResettaTestoBudget();
        }
        private void BudgetSalvaSpesaButton_Click(object sender, EventArgs e)
        {
            int n;
            if (int.TryParse(this.budgetSpesaAccontoTextBox.Text,out n))
            {
                this.budgetSpesePeriodicheComboBox.Items.Add(this.budgetDescrizioneSpesaTextBox.Text);
                this.ResettaTestoBudget();
            }
            else
            {
                MessageBox.Show("L'acconto deve essere intero!");
            }
        }

        #endregion

        #region achievement

        private void AchievementListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var ach = Queries.db.ACHIEVEMENTs.Where(a => a.nome == this.achievementListBox.SelectedItem.ToString()).First();
            var ac = Queries.GetAchievementCompletamentoUtente(this.utente, Decimal.ToInt32(ach.id));
            this.achievementNomeTextBox.Text = this.achievementListBox.Text;
            this.achievementDescrizioneTextBox.Text = ach.descrizione;
            this.achievementCompletamentoTextBox.Text = ac.First().completamento_percentuale.ToString();
        }

        private void Attivita_bAggiungiCategoria_Click(object sender, EventArgs e)
        {
            Queries.AddTipologia(this.attivita_textAggiungiCategoria.Text);
            Attivita_rigeneraComboCategoria();
            this.attivita_bSalva.Enabled = false;
        }

        //---------------------------------------------------------------------------
        #endregion


        #region auto generated funcitons
        private void Attivita_textPremioPP_TextChanged(object sender, EventArgs e)
        {

        }

        private void Attivita_groupBoxPenitenza_Enter(object sender, EventArgs e)
        {

        }

        private void Attivita_comboPenitenza_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LabelComboCategoria_Click(object sender, EventArgs e)
        {

        }

        private void Attivita_textPenitenza_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void Attivita_textCostoEuro_TextChanged(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Attivita_textAggiungiCategoria_TextChanged(object sender, EventArgs e)
        {

        }

        private void SplitContainer3_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Attivita_textPremioPV_TextChanged(object sender, EventArgs e)
        {

        }

        private void CalendarioAttivitaListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #endregion

    }
}

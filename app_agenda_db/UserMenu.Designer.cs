﻿namespace app_agenda_db
{
    partial class UserMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private const int panel1ButtonHeight = 23;
        private const int panel1ButtonWidth = 132;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listaBottoniSinistraTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.b_attivita = new System.Windows.Forms.Button();
            this.b_statistiche = new System.Windows.Forms.Button();
            this.b_obiettivi = new System.Windows.Forms.Button();
            this.b_achievement = new System.Windows.Forms.Button();
            this.b_calendario = new System.Windows.Forms.Button();
            this.b_Budget = new System.Windows.Forms.Button();
            this.calendarioGiornalieroTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.calendarioMeseButton = new System.Windows.Forms.Button();
            this.calendarioAttivitaTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.calendarioAttivitaListBox = new System.Windows.Forms.CheckedListBox();
            this.calendarioGiornalieroDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.pannelloBudget = new System.Windows.Forms.TableLayoutPanel();
            this.budgetSpeseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.budgetSpeseGroupBox = new System.Windows.Forms.GroupBox();
            this.budgetAccontoLabel = new System.Windows.Forms.Label();
            this.budgetSpesaAccontoTextBox = new System.Windows.Forms.TextBox();
            this.budgetDescrizioneLabel = new System.Windows.Forms.Label();
            this.budgetDescrizioneSpesaTextBox = new System.Windows.Forms.TextBox();
            this.budgetSpesePeriodicheComboBox = new System.Windows.Forms.ComboBox();
            this.budgetBottoniTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.budgetAggiungiSpesaButton = new System.Windows.Forms.Button();
            this.budgetSalvaSpesaButton = new System.Windows.Forms.Button();
            this.budgetEliminaSpesaButton = new System.Windows.Forms.Button();
            this.budgetBudgetGroupBox = new System.Windows.Forms.GroupBox();
            this.budgetImpostatoLabel = new System.Windows.Forms.Label();
            this.budgetBudgetTextBox = new System.Windows.Forms.TextBox();
            this.budgetImpostaButton = new System.Windows.Forms.Button();
            this.pannelloStatistiche = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.statisticheSplitContainer = new System.Windows.Forms.SplitContainer();
            this.graficoStatistiche = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.statistiche_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.statistiche_bAnno = new System.Windows.Forms.Button();
            this.statistiche_bMese = new System.Windows.Forms.Button();
            this.statistiche_bSettimana = new System.Windows.Forms.Button();
            this.statistiche_bPV = new System.Windows.Forms.Button();
            this.statistiche_bPP = new System.Windows.Forms.Button();
            this.statistiche_bSpese = new System.Windows.Forms.Button();
            this.statistiche_bAttivita = new System.Windows.Forms.Button();
            this.pannelloAttivita = new System.Windows.Forms.Panel();
            this.SchermInizAttività = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.attivita_groupBoxDescrizione = new System.Windows.Forms.GroupBox();
            this.attivita_textDescrizione = new System.Windows.Forms.TextBox();
            this.attivita_groupBoxNome = new System.Windows.Forms.GroupBox();
            this.attivita_textNome = new System.Windows.Forms.TextBox();
            this.attivita_groupBoxColore = new System.Windows.Forms.GroupBox();
            this.attivita_comboColore = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.attivita_textCostoPV = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.attivita_textCostoEuro = new System.Windows.Forms.TextBox();
            this.attivita_groupBoxPenitenza = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.attivita_comboPenitenza = new System.Windows.Forms.ComboBox();
            this.attivita_textPenitenza = new System.Windows.Forms.TextBox();
            this.attivita_groupBoxPremio = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.attivita_textPremioPV = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.attivita_textPremioPP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.attivita_textPremioEuro = new System.Windows.Forms.TextBox();
            this.attivita_groupBoxCategoria = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelComboCategoria = new System.Windows.Forms.Label();
            this.attivita_textAggiungiCategoria = new System.Windows.Forms.TextBox();
            this.attivita_comboCategoria = new System.Windows.Forms.ComboBox();
            this.attivita_bAggiungiCategoria = new System.Windows.Forms.Button();
            this.durataAttivitaGroupBox = new System.Windows.Forms.GroupBox();
            this.attivitaATimePicker = new System.Windows.Forms.DateTimePicker();
            this.attivitaDaTimePicker = new System.Windows.Forms.DateTimePicker();
            this.attivitaADateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.attivitaDaDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.durataAttivitaALabel = new System.Windows.Forms.Label();
            this.durataAttivitaDaLabel = new System.Windows.Forms.Label();
            this.attivita_comboSelezionaAttivita = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.attivita_bSalva = new System.Windows.Forms.Button();
            this.attivita_bAggiungi = new System.Windows.Forms.Button();
            this.attivita_bElimina = new System.Windows.Forms.Button();
            this.attivita_bAvvia = new System.Windows.Forms.Button();
            this.obiettivo_pannello_iniziale = new System.Windows.Forms.Panel();
            this.obiettivo_split_vista = new System.Windows.Forms.SplitContainer();
            this.obiettivoComboEGroupSplitContainer = new System.Windows.Forms.SplitContainer();
            this.obiettiviLista = new System.Windows.Forms.ComboBox();
            this.obiettivoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.obiettivoDescrizioneGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoDescrizioneTextBox = new System.Windows.Forms.TextBox();
            this.obiettivoSceltaSottoObiettiviGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAggiungiSottoObiettivoButton = new System.Windows.Forms.Button();
            this.obiettivoSceltaSottoObiettivoComboBox = new System.Windows.Forms.ComboBox();
            this.obiettivoDurataGiorniGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAvviaButton = new System.Windows.Forms.Button();
            this.obiettivoALabel = new System.Windows.Forms.Label();
            this.obiettivoDaLabel = new System.Windows.Forms.Label();
            this.obiettivoTermineDurataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.obiettivoInizioDurataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.obiettivoSceltaAttivitaGroupBox = new System.Windows.Forms.GroupBox();
            this.obiettivoAggiungiAttivitaButton = new System.Windows.Forms.Button();
            this.obiettivoNVolteTextBox = new System.Windows.Forms.TextBox();
            this.obiettivoNVolteLabel = new System.Windows.Forms.Label();
            this.obiettivoSceltaAttivitaComboBox = new System.Windows.Forms.ComboBox();
            this.obiettivoBottoniTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.obiettivoSalvaButton = new System.Windows.Forms.Button();
            this.obiettivoEliminaButton = new System.Windows.Forms.Button();
            this.obiettivoAggiungiButton = new System.Windows.Forms.Button();
            this.obiettivoVediButton = new System.Windows.Forms.Button();
            this.pannelloCalendario = new System.Windows.Forms.TableLayoutPanel();
            this.calendarioMensileTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.calendarioMeseGiorno7 = new System.Windows.Forms.TextBox();
            this.calendarioMeseGiorno6 = new System.Windows.Forms.TextBox();
            this.calendarioMeseGiorno5 = new System.Windows.Forms.TextBox();
            this.calendarioMeseGiorno4 = new System.Windows.Forms.TextBox();
            this.calendarioMeseGiorno3 = new System.Windows.Forms.TextBox();
            this.calendarioMeseGiorno2 = new System.Windows.Forms.TextBox();
            this.calendarioBottoneGiorno35 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno34 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno33 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno32 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno31 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno30 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno29 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno28 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno27 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno26 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno25 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno24 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno23 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno22 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno21 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno20 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno19 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno18 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno17 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno16 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno15 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno14 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno13 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno12 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno11 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno10 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno9 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno8 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno7 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno6 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno5 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno4 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno3 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno2 = new System.Windows.Forms.Button();
            this.calendarioBottoneGiorno1 = new System.Windows.Forms.Button();
            this.calendarioMeseGiorno1 = new System.Windows.Forms.TextBox();
            this.calendarioMeseTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.calendarioMeseTextBox = new System.Windows.Forms.TextBox();
            this.calendarioMeseBottoneSinistra = new System.Windows.Forms.Button();
            this.calendarioMeseFrecciaDestra = new System.Windows.Forms.Button();
            this.pannelloAchievement = new System.Windows.Forms.Panel();
            this.achievementSplitContainer = new System.Windows.Forms.SplitContainer();
            this.achievementListBox = new System.Windows.Forms.ListBox();
            this.achievementDescrizioneGroupBox = new System.Windows.Forms.GroupBox();
            this.achievementCompletamentoTextBox = new System.Windows.Forms.TextBox();
            this.achievementCompletamentoLabel = new System.Windows.Forms.Label();
            this.achievementDescrizioneTextBox = new System.Windows.Forms.TextBox();
            this.achievementDescrizioneLabel = new System.Windows.Forms.Label();
            this.achievementNomeTextBox = new System.Windows.Forms.TextBox();
            this.achievementNomeLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.listaBottoniSinistraTableLayoutPanel.SuspendLayout();
            this.calendarioGiornalieroTableLayoutPanel.SuspendLayout();
            this.calendarioAttivitaTableLayoutPanel.SuspendLayout();
            this.pannelloBudget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.budgetSpeseSplitContainer)).BeginInit();
            this.budgetSpeseSplitContainer.Panel1.SuspendLayout();
            this.budgetSpeseSplitContainer.Panel2.SuspendLayout();
            this.budgetSpeseSplitContainer.SuspendLayout();
            this.budgetSpeseGroupBox.SuspendLayout();
            this.budgetBottoniTableLayoutPanel.SuspendLayout();
            this.budgetBudgetGroupBox.SuspendLayout();
            this.pannelloStatistiche.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statisticheSplitContainer)).BeginInit();
            this.statisticheSplitContainer.Panel1.SuspendLayout();
            this.statisticheSplitContainer.Panel2.SuspendLayout();
            this.statisticheSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graficoStatistiche)).BeginInit();
            this.pannelloAttivita.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchermInizAttività)).BeginInit();
            this.SchermInizAttività.Panel1.SuspendLayout();
            this.SchermInizAttività.Panel2.SuspendLayout();
            this.SchermInizAttività.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.attivita_groupBoxDescrizione.SuspendLayout();
            this.attivita_groupBoxNome.SuspendLayout();
            this.attivita_groupBoxColore.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.attivita_groupBoxPenitenza.SuspendLayout();
            this.attivita_groupBoxPremio.SuspendLayout();
            this.attivita_groupBoxCategoria.SuspendLayout();
            this.durataAttivitaGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.obiettivo_pannello_iniziale.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivo_split_vista)).BeginInit();
            this.obiettivo_split_vista.Panel1.SuspendLayout();
            this.obiettivo_split_vista.Panel2.SuspendLayout();
            this.obiettivo_split_vista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivoComboEGroupSplitContainer)).BeginInit();
            this.obiettivoComboEGroupSplitContainer.Panel1.SuspendLayout();
            this.obiettivoComboEGroupSplitContainer.Panel2.SuspendLayout();
            this.obiettivoComboEGroupSplitContainer.SuspendLayout();
            this.obiettivoTableLayoutPanel.SuspendLayout();
            this.obiettivoDescrizioneGroupBox.SuspendLayout();
            this.obiettivoSceltaSottoObiettiviGroupBox.SuspendLayout();
            this.obiettivoDurataGiorniGroupBox.SuspendLayout();
            this.obiettivoSceltaAttivitaGroupBox.SuspendLayout();
            this.obiettivoBottoniTableLayoutPanel.SuspendLayout();
            this.pannelloCalendario.SuspendLayout();
            this.calendarioMensileTableLayoutPanel.SuspendLayout();
            this.calendarioMeseTableLayoutPanel.SuspendLayout();
            this.pannelloAchievement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.achievementSplitContainer)).BeginInit();
            this.achievementSplitContainer.Panel1.SuspendLayout();
            this.achievementSplitContainer.Panel2.SuspendLayout();
            this.achievementSplitContainer.SuspendLayout();
            this.achievementDescrizioneGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listaBottoniSinistraTableLayoutPanel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.calendarioGiornalieroTableLayoutPanel);
            this.splitContainer1.Panel2.Controls.Add(this.pannelloBudget);
            this.splitContainer1.Panel2.Controls.Add(this.pannelloStatistiche);
            this.splitContainer1.Panel2.Controls.Add(this.pannelloAttivita);
            this.splitContainer1.Panel2.Controls.Add(this.obiettivo_pannello_iniziale);
            this.splitContainer1.Panel2.Controls.Add(this.pannelloCalendario);
            this.splitContainer1.Panel2.Controls.Add(this.pannelloAchievement);
            this.splitContainer1.Size = new System.Drawing.Size(1370, 748);
            this.splitContainer1.SplitterDistance = 302;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 0;
            // 
            // listaBottoniSinistraTableLayoutPanel
            // 
            this.listaBottoniSinistraTableLayoutPanel.ColumnCount = 1;
            this.listaBottoniSinistraTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_attivita, 0, 2);
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_statistiche, 0, 1);
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_obiettivi, 0, 4);
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_achievement, 0, 3);
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_calendario, 0, 0);
            this.listaBottoniSinistraTableLayoutPanel.Controls.Add(this.b_Budget, 0, 5);
            this.listaBottoniSinistraTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listaBottoniSinistraTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.listaBottoniSinistraTableLayoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.listaBottoniSinistraTableLayoutPanel.Name = "listaBottoniSinistraTableLayoutPanel";
            this.listaBottoniSinistraTableLayoutPanel.RowCount = 6;
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.listaBottoniSinistraTableLayoutPanel.Size = new System.Drawing.Size(302, 748);
            this.listaBottoniSinistraTableLayoutPanel.TabIndex = 7;
            // 
            // b_attivita
            // 
            this.b_attivita.AutoSize = true;
            this.b_attivita.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_attivita.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_attivita.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_attivita.Location = new System.Drawing.Point(0, 248);
            this.b_attivita.Margin = new System.Windows.Forms.Padding(0);
            this.b_attivita.Name = "b_attivita";
            this.b_attivita.Size = new System.Drawing.Size(302, 124);
            this.b_attivita.TabIndex = 1;
            this.b_attivita.Text = "Attività";
            this.b_attivita.UseVisualStyleBackColor = true;
            this.b_attivita.Click += new System.EventHandler(this.B_Attivita_Click);
            // 
            // b_statistiche
            // 
            this.b_statistiche.AutoSize = true;
            this.b_statistiche.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_statistiche.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_statistiche.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_statistiche.Location = new System.Drawing.Point(0, 124);
            this.b_statistiche.Margin = new System.Windows.Forms.Padding(0);
            this.b_statistiche.Name = "b_statistiche";
            this.b_statistiche.Size = new System.Drawing.Size(302, 124);
            this.b_statistiche.TabIndex = 3;
            this.b_statistiche.Text = "Statistiche";
            this.b_statistiche.UseVisualStyleBackColor = true;
            this.b_statistiche.Click += new System.EventHandler(this.B_Statistiche_Click);
            // 
            // b_obiettivi
            // 
            this.b_obiettivi.AutoSize = true;
            this.b_obiettivi.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_obiettivi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_obiettivi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_obiettivi.Location = new System.Drawing.Point(0, 496);
            this.b_obiettivi.Margin = new System.Windows.Forms.Padding(0);
            this.b_obiettivi.Name = "b_obiettivi";
            this.b_obiettivi.Size = new System.Drawing.Size(302, 124);
            this.b_obiettivi.TabIndex = 2;
            this.b_obiettivi.Text = "Obiettivi";
            this.b_obiettivi.UseVisualStyleBackColor = true;
            this.b_obiettivi.Click += new System.EventHandler(this.B_Obiettivi_Click);
            // 
            // b_achievement
            // 
            this.b_achievement.AutoSize = true;
            this.b_achievement.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_achievement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_achievement.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_achievement.Location = new System.Drawing.Point(0, 372);
            this.b_achievement.Margin = new System.Windows.Forms.Padding(0);
            this.b_achievement.Name = "b_achievement";
            this.b_achievement.Size = new System.Drawing.Size(302, 124);
            this.b_achievement.TabIndex = 5;
            this.b_achievement.Text = "Achievement";
            this.b_achievement.UseVisualStyleBackColor = true;
            this.b_achievement.Click += new System.EventHandler(this.B_Achievement_Click);
            // 
            // b_calendario
            // 
            this.b_calendario.AutoSize = true;
            this.b_calendario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_calendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_calendario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_calendario.Location = new System.Drawing.Point(0, 0);
            this.b_calendario.Margin = new System.Windows.Forms.Padding(0);
            this.b_calendario.Name = "b_calendario";
            this.b_calendario.Size = new System.Drawing.Size(302, 124);
            this.b_calendario.TabIndex = 0;
            this.b_calendario.Text = "Calendario";
            this.b_calendario.UseVisualStyleBackColor = true;
            this.b_calendario.Click += new System.EventHandler(this.B_Calendario_Click);
            // 
            // b_Budget
            // 
            this.b_Budget.AutoSize = true;
            this.b_Budget.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.b_Budget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b_Budget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_Budget.Location = new System.Drawing.Point(0, 620);
            this.b_Budget.Margin = new System.Windows.Forms.Padding(0);
            this.b_Budget.Name = "b_Budget";
            this.b_Budget.Size = new System.Drawing.Size(302, 128);
            this.b_Budget.TabIndex = 6;
            this.b_Budget.Text = "Budget";
            this.b_Budget.UseVisualStyleBackColor = true;
            this.b_Budget.Click += new System.EventHandler(this.B_Budget_Click);
            // 
            // calendarioGiornalieroTableLayoutPanel
            // 
            this.calendarioGiornalieroTableLayoutPanel.AutoSize = true;
            this.calendarioGiornalieroTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioGiornalieroTableLayoutPanel.ColumnCount = 2;
            this.calendarioGiornalieroTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.39098F));
            this.calendarioGiornalieroTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.60902F));
            this.calendarioGiornalieroTableLayoutPanel.Controls.Add(this.calendarioMeseButton, 1, 0);
            this.calendarioGiornalieroTableLayoutPanel.Controls.Add(this.calendarioAttivitaTableLayoutPanel, 0, 0);
            this.calendarioGiornalieroTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioGiornalieroTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.calendarioGiornalieroTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioGiornalieroTableLayoutPanel.Name = "calendarioGiornalieroTableLayoutPanel";
            this.calendarioGiornalieroTableLayoutPanel.RowCount = 1;
            this.calendarioGiornalieroTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.calendarioGiornalieroTableLayoutPanel.Size = new System.Drawing.Size(1066, 748);
            this.calendarioGiornalieroTableLayoutPanel.TabIndex = 2;
            // 
            // calendarioMeseButton
            // 
            this.calendarioMeseButton.AutoSize = true;
            this.calendarioMeseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioMeseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseButton.Location = new System.Drawing.Point(867, 0);
            this.calendarioMeseButton.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseButton.Name = "calendarioMeseButton";
            this.calendarioMeseButton.Size = new System.Drawing.Size(199, 748);
            this.calendarioMeseButton.TabIndex = 0;
            this.calendarioMeseButton.Text = "Mese";
            this.calendarioMeseButton.UseVisualStyleBackColor = true;
            this.calendarioMeseButton.Click += new System.EventHandler(this.CalendarioMeseButton_Click);
            // 
            // calendarioAttivitaTableLayoutPanel
            // 
            this.calendarioAttivitaTableLayoutPanel.AutoSize = true;
            this.calendarioAttivitaTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioAttivitaTableLayoutPanel.ColumnCount = 1;
            this.calendarioAttivitaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.calendarioAttivitaTableLayoutPanel.Controls.Add(this.calendarioAttivitaListBox, 0, 1);
            this.calendarioAttivitaTableLayoutPanel.Controls.Add(this.calendarioGiornalieroDateTimePicker, 0, 0);
            this.calendarioAttivitaTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioAttivitaTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.calendarioAttivitaTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioAttivitaTableLayoutPanel.Name = "calendarioAttivitaTableLayoutPanel";
            this.calendarioAttivitaTableLayoutPanel.RowCount = 2;
            this.calendarioAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.884319F));
            this.calendarioAttivitaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95.11568F));
            this.calendarioAttivitaTableLayoutPanel.Size = new System.Drawing.Size(867, 748);
            this.calendarioAttivitaTableLayoutPanel.TabIndex = 2;
            // 
            // calendarioAttivitaListBox
            // 
            this.calendarioAttivitaListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioAttivitaListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioAttivitaListBox.FormattingEnabled = true;
            this.calendarioAttivitaListBox.Items.AddRange(new object[] {
            "Attività 1",
            "Attività 2"});
            this.calendarioAttivitaListBox.Location = new System.Drawing.Point(0, 36);
            this.calendarioAttivitaListBox.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioAttivitaListBox.Name = "calendarioAttivitaListBox";
            this.calendarioAttivitaListBox.Size = new System.Drawing.Size(867, 712);
            this.calendarioAttivitaListBox.TabIndex = 1;
            this.calendarioAttivitaListBox.SelectedIndexChanged += new System.EventHandler(this.CalendarioAttivitaListBox_SelectedIndexChanged);
            // 
            // calendarioGiornalieroDateTimePicker
            // 
            this.calendarioGiornalieroDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioGiornalieroDateTimePicker.Location = new System.Drawing.Point(0, 0);
            this.calendarioGiornalieroDateTimePicker.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioGiornalieroDateTimePicker.Name = "calendarioGiornalieroDateTimePicker";
            this.calendarioGiornalieroDateTimePicker.Size = new System.Drawing.Size(867, 31);
            this.calendarioGiornalieroDateTimePicker.TabIndex = 2;
            this.calendarioGiornalieroDateTimePicker.ValueChanged += new System.EventHandler(this.CalendarioGiornalieroDateTimePicker_ValueChanged);
            // 
            // pannelloBudget
            // 
            this.pannelloBudget.AutoSize = true;
            this.pannelloBudget.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pannelloBudget.ColumnCount = 2;
            this.pannelloBudget.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pannelloBudget.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pannelloBudget.Controls.Add(this.budgetSpeseSplitContainer, 1, 0);
            this.pannelloBudget.Controls.Add(this.budgetBudgetGroupBox, 0, 0);
            this.pannelloBudget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloBudget.Location = new System.Drawing.Point(0, 0);
            this.pannelloBudget.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.pannelloBudget.Name = "pannelloBudget";
            this.pannelloBudget.RowCount = 1;
            this.pannelloBudget.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pannelloBudget.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 748F));
            this.pannelloBudget.Size = new System.Drawing.Size(1066, 748);
            this.pannelloBudget.TabIndex = 5;
            // 
            // budgetSpeseSplitContainer
            // 
            this.budgetSpeseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetSpeseSplitContainer.Location = new System.Drawing.Point(533, 0);
            this.budgetSpeseSplitContainer.Margin = new System.Windows.Forms.Padding(0);
            this.budgetSpeseSplitContainer.Name = "budgetSpeseSplitContainer";
            this.budgetSpeseSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // budgetSpeseSplitContainer.Panel1
            // 
            this.budgetSpeseSplitContainer.Panel1.Controls.Add(this.budgetSpeseGroupBox);
            // 
            // budgetSpeseSplitContainer.Panel2
            // 
            this.budgetSpeseSplitContainer.Panel2.Controls.Add(this.budgetBottoniTableLayoutPanel);
            this.budgetSpeseSplitContainer.Size = new System.Drawing.Size(533, 748);
            this.budgetSpeseSplitContainer.SplitterDistance = 640;
            this.budgetSpeseSplitContainer.TabIndex = 5;
            // 
            // budgetSpeseGroupBox
            // 
            this.budgetSpeseGroupBox.AutoSize = true;
            this.budgetSpeseGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetSpeseGroupBox.Controls.Add(this.budgetAccontoLabel);
            this.budgetSpeseGroupBox.Controls.Add(this.budgetSpesaAccontoTextBox);
            this.budgetSpeseGroupBox.Controls.Add(this.budgetDescrizioneLabel);
            this.budgetSpeseGroupBox.Controls.Add(this.budgetDescrizioneSpesaTextBox);
            this.budgetSpeseGroupBox.Controls.Add(this.budgetSpesePeriodicheComboBox);
            this.budgetSpeseGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetSpeseGroupBox.Location = new System.Drawing.Point(0, 0);
            this.budgetSpeseGroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.budgetSpeseGroupBox.Name = "budgetSpeseGroupBox";
            this.budgetSpeseGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.budgetSpeseGroupBox.Size = new System.Drawing.Size(533, 640);
            this.budgetSpeseGroupBox.TabIndex = 1;
            this.budgetSpeseGroupBox.TabStop = false;
            this.budgetSpeseGroupBox.Text = "Spese";
            // 
            // budgetAccontoLabel
            // 
            this.budgetAccontoLabel.AutoSize = true;
            this.budgetAccontoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.budgetAccontoLabel.Location = new System.Drawing.Point(108, 196);
            this.budgetAccontoLabel.Margin = new System.Windows.Forms.Padding(0);
            this.budgetAccontoLabel.Name = "budgetAccontoLabel";
            this.budgetAccontoLabel.Size = new System.Drawing.Size(105, 30);
            this.budgetAccontoLabel.TabIndex = 4;
            this.budgetAccontoLabel.Text = "Acconto";
            // 
            // budgetSpesaAccontoTextBox
            // 
            this.budgetSpesaAccontoTextBox.Location = new System.Drawing.Point(94, 227);
            this.budgetSpesaAccontoTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.budgetSpesaAccontoTextBox.Name = "budgetSpesaAccontoTextBox";
            this.budgetSpesaAccontoTextBox.Size = new System.Drawing.Size(80, 31);
            this.budgetSpesaAccontoTextBox.TabIndex = 3;
            this.budgetSpesaAccontoTextBox.TextChanged += new System.EventHandler(this.BudgetSpesaAccontoTextBox_TextChanged);
            // 
            // budgetDescrizioneLabel
            // 
            this.budgetDescrizioneLabel.AutoSize = true;
            this.budgetDescrizioneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.budgetDescrizioneLabel.Location = new System.Drawing.Point(108, 94);
            this.budgetDescrizioneLabel.Margin = new System.Windows.Forms.Padding(0);
            this.budgetDescrizioneLabel.Name = "budgetDescrizioneLabel";
            this.budgetDescrizioneLabel.Size = new System.Drawing.Size(148, 30);
            this.budgetDescrizioneLabel.TabIndex = 2;
            this.budgetDescrizioneLabel.Text = "Descrizione";
            // 
            // budgetDescrizioneSpesaTextBox
            // 
            this.budgetDescrizioneSpesaTextBox.Location = new System.Drawing.Point(80, 127);
            this.budgetDescrizioneSpesaTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.budgetDescrizioneSpesaTextBox.Multiline = true;
            this.budgetDescrizioneSpesaTextBox.Name = "budgetDescrizioneSpesaTextBox";
            this.budgetDescrizioneSpesaTextBox.Size = new System.Drawing.Size(124, 64);
            this.budgetDescrizioneSpesaTextBox.TabIndex = 1;
            this.budgetDescrizioneSpesaTextBox.TextChanged += new System.EventHandler(this.BudgetDescrizioneSpesaTextBox_TextChanged);
            // 
            // budgetSpesePeriodicheComboBox
            // 
            this.budgetSpesePeriodicheComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.budgetSpesePeriodicheComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.budgetSpesePeriodicheComboBox.FormattingEnabled = true;
            this.budgetSpesePeriodicheComboBox.Items.AddRange(new object[] {
            "assad",
            "fh",
            "assad",
            "fh",
            "assad",
            "fh",
            "assad",
            "fh"});
            this.budgetSpesePeriodicheComboBox.Location = new System.Drawing.Point(80, 42);
            this.budgetSpesePeriodicheComboBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.budgetSpesePeriodicheComboBox.Name = "budgetSpesePeriodicheComboBox";
            this.budgetSpesePeriodicheComboBox.Size = new System.Drawing.Size(124, 38);
            this.budgetSpesePeriodicheComboBox.TabIndex = 0;
            this.budgetSpesePeriodicheComboBox.SelectedIndexChanged += new System.EventHandler(this.BudgetSpesePeriodicheComboBox_SelectedIndexChanged);
            // 
            // budgetBottoniTableLayoutPanel
            // 
            this.budgetBottoniTableLayoutPanel.AutoSize = true;
            this.budgetBottoniTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetBottoniTableLayoutPanel.ColumnCount = 3;
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.budgetBottoniTableLayoutPanel.Controls.Add(this.budgetAggiungiSpesaButton, 2, 0);
            this.budgetBottoniTableLayoutPanel.Controls.Add(this.budgetSalvaSpesaButton, 1, 0);
            this.budgetBottoniTableLayoutPanel.Controls.Add(this.budgetEliminaSpesaButton, 0, 0);
            this.budgetBottoniTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetBottoniTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.budgetBottoniTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.budgetBottoniTableLayoutPanel.Name = "budgetBottoniTableLayoutPanel";
            this.budgetBottoniTableLayoutPanel.RowCount = 1;
            this.budgetBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.budgetBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.budgetBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.budgetBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.budgetBottoniTableLayoutPanel.Size = new System.Drawing.Size(533, 104);
            this.budgetBottoniTableLayoutPanel.TabIndex = 0;
            // 
            // budgetAggiungiSpesaButton
            // 
            this.budgetAggiungiSpesaButton.AutoSize = true;
            this.budgetAggiungiSpesaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetAggiungiSpesaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetAggiungiSpesaButton.Enabled = false;
            this.budgetAggiungiSpesaButton.Location = new System.Drawing.Point(354, 0);
            this.budgetAggiungiSpesaButton.Margin = new System.Windows.Forms.Padding(0);
            this.budgetAggiungiSpesaButton.Name = "budgetAggiungiSpesaButton";
            this.budgetAggiungiSpesaButton.Size = new System.Drawing.Size(179, 104);
            this.budgetAggiungiSpesaButton.TabIndex = 2;
            this.budgetAggiungiSpesaButton.Text = "Aggiungi";
            this.budgetAggiungiSpesaButton.UseVisualStyleBackColor = true;
            this.budgetAggiungiSpesaButton.Click += new System.EventHandler(this.BudgetAggiungiSpesaButton_Click);
            // 
            // budgetSalvaSpesaButton
            // 
            this.budgetSalvaSpesaButton.AutoSize = true;
            this.budgetSalvaSpesaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetSalvaSpesaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetSalvaSpesaButton.Enabled = false;
            this.budgetSalvaSpesaButton.Location = new System.Drawing.Point(177, 0);
            this.budgetSalvaSpesaButton.Margin = new System.Windows.Forms.Padding(0);
            this.budgetSalvaSpesaButton.Name = "budgetSalvaSpesaButton";
            this.budgetSalvaSpesaButton.Size = new System.Drawing.Size(177, 104);
            this.budgetSalvaSpesaButton.TabIndex = 1;
            this.budgetSalvaSpesaButton.Text = "Salva";
            this.budgetSalvaSpesaButton.UseVisualStyleBackColor = true;
            this.budgetSalvaSpesaButton.Click += new System.EventHandler(this.BudgetSalvaSpesaButton_Click);
            // 
            // budgetEliminaSpesaButton
            // 
            this.budgetEliminaSpesaButton.AutoSize = true;
            this.budgetEliminaSpesaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetEliminaSpesaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetEliminaSpesaButton.Enabled = false;
            this.budgetEliminaSpesaButton.Location = new System.Drawing.Point(0, 0);
            this.budgetEliminaSpesaButton.Margin = new System.Windows.Forms.Padding(0);
            this.budgetEliminaSpesaButton.Name = "budgetEliminaSpesaButton";
            this.budgetEliminaSpesaButton.Size = new System.Drawing.Size(177, 104);
            this.budgetEliminaSpesaButton.TabIndex = 0;
            this.budgetEliminaSpesaButton.Text = "Elimina";
            this.budgetEliminaSpesaButton.UseVisualStyleBackColor = true;
            this.budgetEliminaSpesaButton.Click += new System.EventHandler(this.BudgetEliminaSpesaButton_Click);
            // 
            // budgetBudgetGroupBox
            // 
            this.budgetBudgetGroupBox.AutoSize = true;
            this.budgetBudgetGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.budgetBudgetGroupBox.Controls.Add(this.budgetImpostatoLabel);
            this.budgetBudgetGroupBox.Controls.Add(this.budgetBudgetTextBox);
            this.budgetBudgetGroupBox.Controls.Add(this.budgetImpostaButton);
            this.budgetBudgetGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.budgetBudgetGroupBox.Location = new System.Drawing.Point(0, 0);
            this.budgetBudgetGroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.budgetBudgetGroupBox.Name = "budgetBudgetGroupBox";
            this.budgetBudgetGroupBox.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.budgetBudgetGroupBox.Size = new System.Drawing.Size(533, 748);
            this.budgetBudgetGroupBox.TabIndex = 0;
            this.budgetBudgetGroupBox.TabStop = false;
            this.budgetBudgetGroupBox.Text = "Budget";
            // 
            // budgetImpostatoLabel
            // 
            this.budgetImpostatoLabel.AutoSize = true;
            this.budgetImpostatoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.budgetImpostatoLabel.Location = new System.Drawing.Point(162, 202);
            this.budgetImpostatoLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.budgetImpostatoLabel.Name = "budgetImpostatoLabel";
            this.budgetImpostatoLabel.Size = new System.Drawing.Size(211, 30);
            this.budgetImpostatoLabel.TabIndex = 2;
            this.budgetImpostatoLabel.Text = "Budget Impostato";
            // 
            // budgetBudgetTextBox
            // 
            this.budgetBudgetTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.budgetBudgetTextBox.Location = new System.Drawing.Point(168, 296);
            this.budgetBudgetTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.budgetBudgetTextBox.Name = "budgetBudgetTextBox";
            this.budgetBudgetTextBox.Size = new System.Drawing.Size(196, 37);
            this.budgetBudgetTextBox.TabIndex = 1;
            this.budgetBudgetTextBox.Text = "Budget";
            // 
            // budgetImpostaButton
            // 
            this.budgetImpostaButton.Location = new System.Drawing.Point(198, 412);
            this.budgetImpostaButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.budgetImpostaButton.Name = "budgetImpostaButton";
            this.budgetImpostaButton.Size = new System.Drawing.Size(150, 44);
            this.budgetImpostaButton.TabIndex = 0;
            this.budgetImpostaButton.Text = "Imposta";
            this.budgetImpostaButton.UseVisualStyleBackColor = true;
            this.budgetImpostaButton.Click += new System.EventHandler(this.BudgetImpostaButton_Click);
            // 
            // pannelloStatistiche
            // 
            this.pannelloStatistiche.Controls.Add(this.panel2);
            this.pannelloStatistiche.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloStatistiche.Location = new System.Drawing.Point(0, 0);
            this.pannelloStatistiche.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.pannelloStatistiche.Name = "pannelloStatistiche";
            this.pannelloStatistiche.Size = new System.Drawing.Size(1066, 748);
            this.pannelloStatistiche.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1066, 748);
            this.panel2.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.statisticheSplitContainer);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.statistiche_bPV);
            this.splitContainer2.Panel2.Controls.Add(this.statistiche_bPP);
            this.splitContainer2.Panel2.Controls.Add(this.statistiche_bSpese);
            this.splitContainer2.Panel2.Controls.Add(this.statistiche_bAttivita);
            this.splitContainer2.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer2.Size = new System.Drawing.Size(1066, 748);
            this.splitContainer2.SplitterDistance = 569;
            this.splitContainer2.SplitterWidth = 8;
            this.splitContainer2.TabIndex = 0;
            // 
            // statisticheSplitContainer
            // 
            this.statisticheSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statisticheSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.statisticheSplitContainer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statisticheSplitContainer.Name = "statisticheSplitContainer";
            // 
            // statisticheSplitContainer.Panel1
            // 
            this.statisticheSplitContainer.Panel1.Controls.Add(this.graficoStatistiche);
            // 
            // statisticheSplitContainer.Panel2
            // 
            this.statisticheSplitContainer.Panel2.Controls.Add(this.statistiche_dateTimePicker);
            this.statisticheSplitContainer.Panel2.Controls.Add(this.label9);
            this.statisticheSplitContainer.Panel2.Controls.Add(this.statistiche_bAnno);
            this.statisticheSplitContainer.Panel2.Controls.Add(this.statistiche_bMese);
            this.statisticheSplitContainer.Panel2.Controls.Add(this.statistiche_bSettimana);
            this.statisticheSplitContainer.Size = new System.Drawing.Size(1066, 569);
            this.statisticheSplitContainer.SplitterDistance = 804;
            this.statisticheSplitContainer.SplitterWidth = 16;
            this.statisticheSplitContainer.TabIndex = 0;
            // 
            // graficoStatistiche
            // 
            chartArea1.Name = "ChartArea1";
            this.graficoStatistiche.ChartAreas.Add(chartArea1);
            this.graficoStatistiche.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.graficoStatistiche.Legends.Add(legend1);
            this.graficoStatistiche.Location = new System.Drawing.Point(0, 0);
            this.graficoStatistiche.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.graficoStatistiche.Name = "graficoStatistiche";
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Blue;
            series1.Legend = "Legend1";
            series1.MarkerColor = System.Drawing.Color.Red;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series1.Name = "plot";
            this.graficoStatistiche.Series.Add(series1);
            this.graficoStatistiche.Size = new System.Drawing.Size(804, 569);
            this.graficoStatistiche.TabIndex = 0;
            this.graficoStatistiche.Text = "chart1";
            // 
            // statistiche_dateTimePicker
            // 
            this.statistiche_dateTimePicker.Location = new System.Drawing.Point(6, 1329);
            this.statistiche_dateTimePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_dateTimePicker.Name = "statistiche_dateTimePicker";
            this.statistiche_dateTimePicker.Size = new System.Drawing.Size(518, 31);
            this.statistiche_dateTimePicker.TabIndex = 1;
            this.statistiche_dateTimePicker.ValueChanged += new System.EventHandler(this.Statistiche_dateTimePicker_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 1244);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 25);
            this.label9.TabIndex = 4;
            this.label9.Text = "data inizio";
            // 
            // statistiche_bAnno
            // 
            this.statistiche_bAnno.AutoSize = true;
            this.statistiche_bAnno.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bAnno.Dock = System.Windows.Forms.DockStyle.Top;
            this.statistiche_bAnno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bAnno.Location = new System.Drawing.Point(0, 82);
            this.statistiche_bAnno.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bAnno.Name = "statistiche_bAnno";
            this.statistiche_bAnno.Size = new System.Drawing.Size(246, 41);
            this.statistiche_bAnno.TabIndex = 2;
            this.statistiche_bAnno.Text = "anno";
            this.statistiche_bAnno.UseVisualStyleBackColor = true;
            this.statistiche_bAnno.Click += new System.EventHandler(this.Statistiche_bAnno_Click);
            // 
            // statistiche_bMese
            // 
            this.statistiche_bMese.AutoSize = true;
            this.statistiche_bMese.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bMese.Dock = System.Windows.Forms.DockStyle.Top;
            this.statistiche_bMese.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bMese.Location = new System.Drawing.Point(0, 41);
            this.statistiche_bMese.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bMese.Name = "statistiche_bMese";
            this.statistiche_bMese.Size = new System.Drawing.Size(246, 41);
            this.statistiche_bMese.TabIndex = 1;
            this.statistiche_bMese.Text = "mese";
            this.statistiche_bMese.UseVisualStyleBackColor = true;
            this.statistiche_bMese.Click += new System.EventHandler(this.Statistiche_bMese_Click);
            // 
            // statistiche_bSettimana
            // 
            this.statistiche_bSettimana.AutoSize = true;
            this.statistiche_bSettimana.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bSettimana.Dock = System.Windows.Forms.DockStyle.Top;
            this.statistiche_bSettimana.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bSettimana.Location = new System.Drawing.Point(0, 0);
            this.statistiche_bSettimana.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bSettimana.Name = "statistiche_bSettimana";
            this.statistiche_bSettimana.Size = new System.Drawing.Size(246, 41);
            this.statistiche_bSettimana.TabIndex = 0;
            this.statistiche_bSettimana.Text = "settimana\r\n";
            this.statistiche_bSettimana.UseVisualStyleBackColor = true;
            this.statistiche_bSettimana.Click += new System.EventHandler(this.Statistiche_bSettimana_Click);
            // 
            // statistiche_bPV
            // 
            this.statistiche_bPV.AutoSize = true;
            this.statistiche_bPV.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bPV.Dock = System.Windows.Forms.DockStyle.Left;
            this.statistiche_bPV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bPV.Location = new System.Drawing.Point(438, 0);
            this.statistiche_bPV.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bPV.Name = "statistiche_bPV";
            this.statistiche_bPV.Size = new System.Drawing.Size(185, 171);
            this.statistiche_bPV.TabIndex = 3;
            this.statistiche_bPV.Text = "Punti Volontà";
            this.statistiche_bPV.UseVisualStyleBackColor = true;
            this.statistiche_bPV.Click += new System.EventHandler(this.Statistiche_bPV_Click);
            // 
            // statistiche_bPP
            // 
            this.statistiche_bPP.AutoSize = true;
            this.statistiche_bPP.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bPP.Dock = System.Windows.Forms.DockStyle.Left;
            this.statistiche_bPP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bPP.Location = new System.Drawing.Point(208, 0);
            this.statistiche_bPP.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bPP.Name = "statistiche_bPP";
            this.statistiche_bPP.Size = new System.Drawing.Size(230, 171);
            this.statistiche_bPP.TabIndex = 2;
            this.statistiche_bPP.Text = "Punti Produttività";
            this.statistiche_bPP.UseVisualStyleBackColor = true;
            this.statistiche_bPP.Click += new System.EventHandler(this.Statistiche_bPP_Click);
            // 
            // statistiche_bSpese
            // 
            this.statistiche_bSpese.AutoSize = true;
            this.statistiche_bSpese.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bSpese.Dock = System.Windows.Forms.DockStyle.Left;
            this.statistiche_bSpese.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bSpese.Location = new System.Drawing.Point(107, 0);
            this.statistiche_bSpese.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.statistiche_bSpese.Name = "statistiche_bSpese";
            this.statistiche_bSpese.Size = new System.Drawing.Size(101, 171);
            this.statistiche_bSpese.TabIndex = 0;
            this.statistiche_bSpese.Text = "Spese";
            this.statistiche_bSpese.UseVisualStyleBackColor = true;
            this.statistiche_bSpese.Click += new System.EventHandler(this.Statistiche_bSpese_Click);
            // 
            // statistiche_bAttivita
            // 
            this.statistiche_bAttivita.AutoSize = true;
            this.statistiche_bAttivita.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statistiche_bAttivita.Dock = System.Windows.Forms.DockStyle.Left;
            this.statistiche_bAttivita.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statistiche_bAttivita.Location = new System.Drawing.Point(0, 0);
            this.statistiche_bAttivita.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.statistiche_bAttivita.Name = "statistiche_bAttivita";
            this.statistiche_bAttivita.Size = new System.Drawing.Size(107, 171);
            this.statistiche_bAttivita.TabIndex = 1;
            this.statistiche_bAttivita.Text = "Attività";
            this.statistiche_bAttivita.UseVisualStyleBackColor = true;
            this.statistiche_bAttivita.Click += new System.EventHandler(this.Statistiche_bAttivita_Click);
            // 
            // pannelloAttivita
            // 
            this.pannelloAttivita.AutoSize = true;
            this.pannelloAttivita.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pannelloAttivita.Controls.Add(this.SchermInizAttività);
            this.pannelloAttivita.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloAttivita.Location = new System.Drawing.Point(0, 0);
            this.pannelloAttivita.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.pannelloAttivita.Name = "pannelloAttivita";
            this.pannelloAttivita.Size = new System.Drawing.Size(1066, 748);
            this.pannelloAttivita.TabIndex = 1;
            // 
            // SchermInizAttività
            // 
            this.SchermInizAttività.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SchermInizAttività.Location = new System.Drawing.Point(0, 0);
            this.SchermInizAttività.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SchermInizAttività.Name = "SchermInizAttività";
            this.SchermInizAttività.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SchermInizAttività.Panel1
            // 
            this.SchermInizAttività.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.SchermInizAttività.Panel1.Controls.Add(this.attivita_comboSelezionaAttivita);
            // 
            // SchermInizAttività.Panel2
            // 
            this.SchermInizAttività.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.SchermInizAttività.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SchermInizAttività.Size = new System.Drawing.Size(1066, 748);
            this.SchermInizAttività.SplitterDistance = 690;
            this.SchermInizAttività.SplitterWidth = 8;
            this.SchermInizAttività.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxDescrizione, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxNome, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxColore, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox7, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxPenitenza, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxPremio, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.attivita_groupBoxCategoria, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.durataAttivitaGroupBox, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.45428F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.92331F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.79351F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.40351F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1066, 657);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // attivita_groupBoxDescrizione
            // 
            this.attivita_groupBoxDescrizione.AutoSize = true;
            this.attivita_groupBoxDescrizione.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxDescrizione.Controls.Add(this.attivita_textDescrizione);
            this.attivita_groupBoxDescrizione.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxDescrizione.Location = new System.Drawing.Point(6, 520);
            this.attivita_groupBoxDescrizione.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxDescrizione.Name = "attivita_groupBoxDescrizione";
            this.attivita_groupBoxDescrizione.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxDescrizione.Size = new System.Drawing.Size(521, 131);
            this.attivita_groupBoxDescrizione.TabIndex = 18;
            this.attivita_groupBoxDescrizione.TabStop = false;
            this.attivita_groupBoxDescrizione.Text = "Descrizione*";
            // 
            // attivita_textDescrizione
            // 
            this.attivita_textDescrizione.Location = new System.Drawing.Point(14, 44);
            this.attivita_textDescrizione.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textDescrizione.Multiline = true;
            this.attivita_textDescrizione.Name = "attivita_textDescrizione";
            this.attivita_textDescrizione.Size = new System.Drawing.Size(462, 75);
            this.attivita_textDescrizione.TabIndex = 6;
            // 
            // attivita_groupBoxNome
            // 
            this.attivita_groupBoxNome.AutoSize = true;
            this.attivita_groupBoxNome.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxNome.Controls.Add(this.attivita_textNome);
            this.attivita_groupBoxNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxNome.Location = new System.Drawing.Point(6, 6);
            this.attivita_groupBoxNome.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxNome.Name = "attivita_groupBoxNome";
            this.attivita_groupBoxNome.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxNome.Size = new System.Drawing.Size(521, 83);
            this.attivita_groupBoxNome.TabIndex = 9;
            this.attivita_groupBoxNome.TabStop = false;
            this.attivita_groupBoxNome.Text = "Nome*";
            // 
            // attivita_textNome
            // 
            this.attivita_textNome.Location = new System.Drawing.Point(8, 35);
            this.attivita_textNome.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textNome.Name = "attivita_textNome";
            this.attivita_textNome.Size = new System.Drawing.Size(182, 31);
            this.attivita_textNome.TabIndex = 6;
            this.attivita_textNome.TextChanged += new System.EventHandler(this.Attivita_textNome_TextChanged);
            // 
            // attivita_groupBoxColore
            // 
            this.attivita_groupBoxColore.AutoSize = true;
            this.attivita_groupBoxColore.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxColore.Controls.Add(this.attivita_comboColore);
            this.attivita_groupBoxColore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxColore.Location = new System.Drawing.Point(539, 6);
            this.attivita_groupBoxColore.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxColore.Name = "attivita_groupBoxColore";
            this.attivita_groupBoxColore.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxColore.Size = new System.Drawing.Size(521, 83);
            this.attivita_groupBoxColore.TabIndex = 17;
            this.attivita_groupBoxColore.TabStop = false;
            this.attivita_groupBoxColore.Text = "Colore*";
            // 
            // attivita_comboColore
            // 
            this.attivita_comboColore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.attivita_comboColore.FormattingEnabled = true;
            this.attivita_comboColore.Location = new System.Drawing.Point(24, 37);
            this.attivita_comboColore.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_comboColore.Name = "attivita_comboColore";
            this.attivita_comboColore.Size = new System.Drawing.Size(194, 33);
            this.attivita_comboColore.TabIndex = 0;
            this.attivita_comboColore.SelectedIndexChanged += new System.EventHandler(this.Attivita_comboColore_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.AutoSize = true;
            this.groupBox7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.attivita_textCostoPV);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.attivita_textCostoEuro);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(539, 324);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox7.Size = new System.Drawing.Size(521, 184);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Costo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(254, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Punti Volontà";
            // 
            // attivita_textCostoPV
            // 
            this.attivita_textCostoPV.Location = new System.Drawing.Point(260, 85);
            this.attivita_textCostoPV.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textCostoPV.Name = "attivita_textCostoPV";
            this.attivita_textCostoPV.Size = new System.Drawing.Size(180, 31);
            this.attivita_textCostoPV.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Euro";
            // 
            // attivita_textCostoEuro
            // 
            this.attivita_textCostoEuro.Location = new System.Drawing.Point(12, 83);
            this.attivita_textCostoEuro.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textCostoEuro.Name = "attivita_textCostoEuro";
            this.attivita_textCostoEuro.Size = new System.Drawing.Size(180, 31);
            this.attivita_textCostoEuro.TabIndex = 6;
            this.attivita_textCostoEuro.TextChanged += new System.EventHandler(this.Attivita_textCostoEuro_TextChanged);
            // 
            // attivita_groupBoxPenitenza
            // 
            this.attivita_groupBoxPenitenza.AutoSize = true;
            this.attivita_groupBoxPenitenza.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxPenitenza.Controls.Add(this.label8);
            this.attivita_groupBoxPenitenza.Controls.Add(this.label7);
            this.attivita_groupBoxPenitenza.Controls.Add(this.attivita_comboPenitenza);
            this.attivita_groupBoxPenitenza.Controls.Add(this.attivita_textPenitenza);
            this.attivita_groupBoxPenitenza.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxPenitenza.Location = new System.Drawing.Point(539, 101);
            this.attivita_groupBoxPenitenza.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxPenitenza.Name = "attivita_groupBoxPenitenza";
            this.attivita_groupBoxPenitenza.Padding = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.attivita_groupBoxPenitenza.Size = new System.Drawing.Size(521, 211);
            this.attivita_groupBoxPenitenza.TabIndex = 13;
            this.attivita_groupBoxPenitenza.TabStop = false;
            this.attivita_groupBoxPenitenza.Text = "Penitenza";
            this.attivita_groupBoxPenitenza.Enter += new System.EventHandler(this.Attivita_groupBoxPenitenza_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(322, 85);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 25);
            this.label8.TabIndex = 13;
            this.label8.Text = "Descrizione";
            this.label8.Click += new System.EventHandler(this.Label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 37);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 25);
            this.label7.TabIndex = 12;
            this.label7.Text = "Seleziona";
            // 
            // attivita_comboPenitenza
            // 
            this.attivita_comboPenitenza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.attivita_comboPenitenza.FormattingEnabled = true;
            this.attivita_comboPenitenza.Location = new System.Drawing.Point(24, 69);
            this.attivita_comboPenitenza.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.attivita_comboPenitenza.Name = "attivita_comboPenitenza";
            this.attivita_comboPenitenza.Size = new System.Drawing.Size(194, 33);
            this.attivita_comboPenitenza.TabIndex = 10;
            this.attivita_comboPenitenza.SelectedIndexChanged += new System.EventHandler(this.Attivita_comboPenitenza_SelectedIndexChanged);
            // 
            // attivita_textPenitenza
            // 
            this.attivita_textPenitenza.Location = new System.Drawing.Point(24, 133);
            this.attivita_textPenitenza.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.attivita_textPenitenza.Multiline = true;
            this.attivita_textPenitenza.Name = "attivita_textPenitenza";
            this.attivita_textPenitenza.Size = new System.Drawing.Size(418, 66);
            this.attivita_textPenitenza.TabIndex = 6;
            this.attivita_textPenitenza.TextChanged += new System.EventHandler(this.Attivita_textPenitenza_TextChanged);
            // 
            // attivita_groupBoxPremio
            // 
            this.attivita_groupBoxPremio.AutoSize = true;
            this.attivita_groupBoxPremio.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxPremio.Controls.Add(this.label4);
            this.attivita_groupBoxPremio.Controls.Add(this.attivita_textPremioPV);
            this.attivita_groupBoxPremio.Controls.Add(this.label5);
            this.attivita_groupBoxPremio.Controls.Add(this.attivita_textPremioPP);
            this.attivita_groupBoxPremio.Controls.Add(this.label6);
            this.attivita_groupBoxPremio.Controls.Add(this.attivita_textPremioEuro);
            this.attivita_groupBoxPremio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxPremio.Location = new System.Drawing.Point(6, 324);
            this.attivita_groupBoxPremio.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxPremio.Name = "attivita_groupBoxPremio";
            this.attivita_groupBoxPremio.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxPremio.Size = new System.Drawing.Size(521, 184);
            this.attivita_groupBoxPremio.TabIndex = 16;
            this.attivita_groupBoxPremio.TabStop = false;
            this.attivita_groupBoxPremio.Text = "Premio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 196);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Punti Volontà";
            // 
            // attivita_textPremioPV
            // 
            this.attivita_textPremioPV.Location = new System.Drawing.Point(6, 238);
            this.attivita_textPremioPV.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textPremioPV.Name = "attivita_textPremioPV";
            this.attivita_textPremioPV.Size = new System.Drawing.Size(436, 31);
            this.attivita_textPremioPV.TabIndex = 10;
            this.attivita_textPremioPV.TextChanged += new System.EventHandler(this.Attivita_textPremioPV_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(224, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Punti Produttività";
            // 
            // attivita_textPremioPP
            // 
            this.attivita_textPremioPP.Location = new System.Drawing.Point(230, 65);
            this.attivita_textPremioPP.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textPremioPP.Name = "attivita_textPremioPP";
            this.attivita_textPremioPP.Size = new System.Drawing.Size(194, 31);
            this.attivita_textPremioPP.TabIndex = 8;
            this.attivita_textPremioPP.TextChanged += new System.EventHandler(this.Attivita_textPremioPP_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(6, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Euro";
            // 
            // attivita_textPremioEuro
            // 
            this.attivita_textPremioEuro.Location = new System.Drawing.Point(8, 62);
            this.attivita_textPremioEuro.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textPremioEuro.Name = "attivita_textPremioEuro";
            this.attivita_textPremioEuro.Size = new System.Drawing.Size(182, 31);
            this.attivita_textPremioEuro.TabIndex = 6;
            // 
            // attivita_groupBoxCategoria
            // 
            this.attivita_groupBoxCategoria.AutoSize = true;
            this.attivita_groupBoxCategoria.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_groupBoxCategoria.Controls.Add(this.label3);
            this.attivita_groupBoxCategoria.Controls.Add(this.LabelComboCategoria);
            this.attivita_groupBoxCategoria.Controls.Add(this.attivita_textAggiungiCategoria);
            this.attivita_groupBoxCategoria.Controls.Add(this.attivita_comboCategoria);
            this.attivita_groupBoxCategoria.Controls.Add(this.attivita_bAggiungiCategoria);
            this.attivita_groupBoxCategoria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_groupBoxCategoria.Location = new System.Drawing.Point(6, 101);
            this.attivita_groupBoxCategoria.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxCategoria.Name = "attivita_groupBoxCategoria";
            this.attivita_groupBoxCategoria.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_groupBoxCategoria.Size = new System.Drawing.Size(521, 211);
            this.attivita_groupBoxCategoria.TabIndex = 12;
            this.attivita_groupBoxCategoria.TabStop = false;
            this.attivita_groupBoxCategoria.Text = "Categoria*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Nuova";
            // 
            // LabelComboCategoria
            // 
            this.LabelComboCategoria.AutoSize = true;
            this.LabelComboCategoria.Location = new System.Drawing.Point(26, 33);
            this.LabelComboCategoria.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LabelComboCategoria.Name = "LabelComboCategoria";
            this.LabelComboCategoria.Size = new System.Drawing.Size(58, 25);
            this.LabelComboCategoria.TabIndex = 11;
            this.LabelComboCategoria.Text = "Lista";
            this.LabelComboCategoria.Click += new System.EventHandler(this.LabelComboCategoria_Click);
            // 
            // attivita_textAggiungiCategoria
            // 
            this.attivita_textAggiungiCategoria.Location = new System.Drawing.Point(32, 142);
            this.attivita_textAggiungiCategoria.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_textAggiungiCategoria.Name = "attivita_textAggiungiCategoria";
            this.attivita_textAggiungiCategoria.Size = new System.Drawing.Size(224, 31);
            this.attivita_textAggiungiCategoria.TabIndex = 10;
            this.attivita_textAggiungiCategoria.TextChanged += new System.EventHandler(this.Attivita_textAggiungiCategoria_TextChanged);
            // 
            // attivita_comboCategoria
            // 
            this.attivita_comboCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.attivita_comboCategoria.FormattingEnabled = true;
            this.attivita_comboCategoria.Location = new System.Drawing.Point(32, 60);
            this.attivita_comboCategoria.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_comboCategoria.Name = "attivita_comboCategoria";
            this.attivita_comboCategoria.Size = new System.Drawing.Size(224, 33);
            this.attivita_comboCategoria.TabIndex = 9;
            this.attivita_comboCategoria.SelectedIndexChanged += new System.EventHandler(this.Attivita_comboCategoria_SelectedIndexChanged);
            // 
            // attivita_bAggiungiCategoria
            // 
            this.attivita_bAggiungiCategoria.AutoSize = true;
            this.attivita_bAggiungiCategoria.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_bAggiungiCategoria.Location = new System.Drawing.Point(328, 102);
            this.attivita_bAggiungiCategoria.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_bAggiungiCategoria.Name = "attivita_bAggiungiCategoria";
            this.attivita_bAggiungiCategoria.Size = new System.Drawing.Size(106, 35);
            this.attivita_bAggiungiCategoria.TabIndex = 8;
            this.attivita_bAggiungiCategoria.Text = "Aggiungi";
            this.attivita_bAggiungiCategoria.UseVisualStyleBackColor = true;
            this.attivita_bAggiungiCategoria.Click += new System.EventHandler(this.Attivita_bAggiungiCategoria_Click);
            // 
            // durataAttivitaGroupBox
            // 
            this.durataAttivitaGroupBox.AutoSize = true;
            this.durataAttivitaGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.durataAttivitaGroupBox.Controls.Add(this.attivitaATimePicker);
            this.durataAttivitaGroupBox.Controls.Add(this.attivitaDaTimePicker);
            this.durataAttivitaGroupBox.Controls.Add(this.attivitaADateTimePicker);
            this.durataAttivitaGroupBox.Controls.Add(this.attivitaDaDateTimePicker);
            this.durataAttivitaGroupBox.Controls.Add(this.durataAttivitaALabel);
            this.durataAttivitaGroupBox.Controls.Add(this.durataAttivitaDaLabel);
            this.durataAttivitaGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.durataAttivitaGroupBox.Location = new System.Drawing.Point(539, 520);
            this.durataAttivitaGroupBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.durataAttivitaGroupBox.Name = "durataAttivitaGroupBox";
            this.durataAttivitaGroupBox.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.durataAttivitaGroupBox.Size = new System.Drawing.Size(521, 131);
            this.durataAttivitaGroupBox.TabIndex = 19;
            this.durataAttivitaGroupBox.TabStop = false;
            this.durataAttivitaGroupBox.Text = "Durata";
            // 
            // attivitaATimePicker
            // 
            this.attivitaATimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.attivitaATimePicker.Location = new System.Drawing.Point(272, 98);
            this.attivitaATimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.attivitaATimePicker.Name = "attivitaATimePicker";
            this.attivitaATimePicker.ShowUpDown = true;
            this.attivitaATimePicker.Size = new System.Drawing.Size(200, 31);
            this.attivitaATimePicker.TabIndex = 5;
            // 
            // attivitaDaTimePicker
            // 
            this.attivitaDaTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.attivitaDaTimePicker.Location = new System.Drawing.Point(8, 98);
            this.attivitaDaTimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.attivitaDaTimePicker.Name = "attivitaDaTimePicker";
            this.attivitaDaTimePicker.ShowUpDown = true;
            this.attivitaDaTimePicker.Size = new System.Drawing.Size(200, 31);
            this.attivitaDaTimePicker.TabIndex = 4;
            // 
            // attivitaADateTimePicker
            // 
            this.attivitaADateTimePicker.Enabled = false;
            this.attivitaADateTimePicker.Location = new System.Drawing.Point(272, 54);
            this.attivitaADateTimePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivitaADateTimePicker.Name = "attivitaADateTimePicker";
            this.attivitaADateTimePicker.Size = new System.Drawing.Size(238, 31);
            this.attivitaADateTimePicker.TabIndex = 3;
            // 
            // attivitaDaDateTimePicker
            // 
            this.attivitaDaDateTimePicker.Enabled = false;
            this.attivitaDaDateTimePicker.Location = new System.Drawing.Point(8, 54);
            this.attivitaDaDateTimePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivitaDaDateTimePicker.Name = "attivitaDaDateTimePicker";
            this.attivitaDaDateTimePicker.Size = new System.Drawing.Size(238, 31);
            this.attivitaDaDateTimePicker.TabIndex = 2;
            // 
            // durataAttivitaALabel
            // 
            this.durataAttivitaALabel.AutoSize = true;
            this.durataAttivitaALabel.Location = new System.Drawing.Point(268, 23);
            this.durataAttivitaALabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.durataAttivitaALabel.Name = "durataAttivitaALabel";
            this.durataAttivitaALabel.Size = new System.Drawing.Size(26, 25);
            this.durataAttivitaALabel.TabIndex = 1;
            this.durataAttivitaALabel.Text = "A";
            // 
            // durataAttivitaDaLabel
            // 
            this.durataAttivitaDaLabel.AutoSize = true;
            this.durataAttivitaDaLabel.Location = new System.Drawing.Point(10, 23);
            this.durataAttivitaDaLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.durataAttivitaDaLabel.Name = "durataAttivitaDaLabel";
            this.durataAttivitaDaLabel.Size = new System.Drawing.Size(39, 25);
            this.durataAttivitaDaLabel.TabIndex = 0;
            this.durataAttivitaDaLabel.Text = "Da";
            // 
            // attivita_comboSelezionaAttivita
            // 
            this.attivita_comboSelezionaAttivita.Dock = System.Windows.Forms.DockStyle.Top;
            this.attivita_comboSelezionaAttivita.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.attivita_comboSelezionaAttivita.FormattingEnabled = true;
            this.attivita_comboSelezionaAttivita.Location = new System.Drawing.Point(0, 0);
            this.attivita_comboSelezionaAttivita.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.attivita_comboSelezionaAttivita.Name = "attivita_comboSelezionaAttivita";
            this.attivita_comboSelezionaAttivita.Size = new System.Drawing.Size(1066, 33);
            this.attivita_comboSelezionaAttivita.TabIndex = 0;
            this.attivita_comboSelezionaAttivita.SelectedIndexChanged += new System.EventHandler(this.Attivita_ComboBoxListaAttivita_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.attivita_bSalva, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.attivita_bAggiungi, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.attivita_bElimina, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.attivita_bAvvia, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1066, 50);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // attivita_bSalva
            // 
            this.attivita_bSalva.AutoSize = true;
            this.attivita_bSalva.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_bSalva.Enabled = false;
            this.attivita_bSalva.Location = new System.Drawing.Point(532, 0);
            this.attivita_bSalva.Margin = new System.Windows.Forms.Padding(0);
            this.attivita_bSalva.Name = "attivita_bSalva";
            this.attivita_bSalva.Size = new System.Drawing.Size(266, 50);
            this.attivita_bSalva.TabIndex = 3;
            this.attivita_bSalva.Text = "Salva Attività";
            this.attivita_bSalva.UseVisualStyleBackColor = true;
            this.attivita_bSalva.Click += new System.EventHandler(this.Attivita_bSalva_Click);
            // 
            // attivita_bAggiungi
            // 
            this.attivita_bAggiungi.AutoSize = true;
            this.attivita_bAggiungi.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_bAggiungi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_bAggiungi.Enabled = false;
            this.attivita_bAggiungi.Location = new System.Drawing.Point(798, 0);
            this.attivita_bAggiungi.Margin = new System.Windows.Forms.Padding(0);
            this.attivita_bAggiungi.Name = "attivita_bAggiungi";
            this.attivita_bAggiungi.Size = new System.Drawing.Size(268, 50);
            this.attivita_bAggiungi.TabIndex = 2;
            this.attivita_bAggiungi.Text = "Aggiungi Attività";
            this.attivita_bAggiungi.UseVisualStyleBackColor = true;
            this.attivita_bAggiungi.Click += new System.EventHandler(this.Attivita_bAggiungi_Click);
            // 
            // attivita_bElimina
            // 
            this.attivita_bElimina.AutoSize = true;
            this.attivita_bElimina.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_bElimina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_bElimina.Enabled = false;
            this.attivita_bElimina.Location = new System.Drawing.Point(266, 0);
            this.attivita_bElimina.Margin = new System.Windows.Forms.Padding(0);
            this.attivita_bElimina.Name = "attivita_bElimina";
            this.attivita_bElimina.Size = new System.Drawing.Size(266, 50);
            this.attivita_bElimina.TabIndex = 4;
            this.attivita_bElimina.Text = "Elimina Attività";
            this.attivita_bElimina.UseVisualStyleBackColor = true;
            this.attivita_bElimina.Click += new System.EventHandler(this.Attivita_bElimina_Click);
            // 
            // attivita_bAvvia
            // 
            this.attivita_bAvvia.AutoSize = true;
            this.attivita_bAvvia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.attivita_bAvvia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attivita_bAvvia.Enabled = false;
            this.attivita_bAvvia.Location = new System.Drawing.Point(0, 0);
            this.attivita_bAvvia.Margin = new System.Windows.Forms.Padding(0);
            this.attivita_bAvvia.Name = "attivita_bAvvia";
            this.attivita_bAvvia.Size = new System.Drawing.Size(266, 50);
            this.attivita_bAvvia.TabIndex = 5;
            this.attivita_bAvvia.Text = "Avvia Attività";
            this.attivita_bAvvia.UseVisualStyleBackColor = true;
            this.attivita_bAvvia.Click += new System.EventHandler(this.Attivita_bAvvia_Click);
            // 
            // obiettivo_pannello_iniziale
            // 
            this.obiettivo_pannello_iniziale.AutoSize = true;
            this.obiettivo_pannello_iniziale.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivo_pannello_iniziale.Controls.Add(this.obiettivo_split_vista);
            this.obiettivo_pannello_iniziale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivo_pannello_iniziale.Location = new System.Drawing.Point(0, 0);
            this.obiettivo_pannello_iniziale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivo_pannello_iniziale.Name = "obiettivo_pannello_iniziale";
            this.obiettivo_pannello_iniziale.Size = new System.Drawing.Size(1066, 748);
            this.obiettivo_pannello_iniziale.TabIndex = 1;
            // 
            // obiettivo_split_vista
            // 
            this.obiettivo_split_vista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivo_split_vista.Location = new System.Drawing.Point(0, 0);
            this.obiettivo_split_vista.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivo_split_vista.Name = "obiettivo_split_vista";
            this.obiettivo_split_vista.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // obiettivo_split_vista.Panel1
            // 
            this.obiettivo_split_vista.Panel1.Controls.Add(this.obiettivoComboEGroupSplitContainer);
            // 
            // obiettivo_split_vista.Panel2
            // 
            this.obiettivo_split_vista.Panel2.Controls.Add(this.obiettivoBottoniTableLayoutPanel);
            this.obiettivo_split_vista.Size = new System.Drawing.Size(1066, 748);
            this.obiettivo_split_vista.SplitterDistance = 628;
            this.obiettivo_split_vista.TabIndex = 0;
            // 
            // obiettivoComboEGroupSplitContainer
            // 
            this.obiettivoComboEGroupSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoComboEGroupSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.obiettivoComboEGroupSplitContainer.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.obiettivoComboEGroupSplitContainer.Name = "obiettivoComboEGroupSplitContainer";
            this.obiettivoComboEGroupSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // obiettivoComboEGroupSplitContainer.Panel1
            // 
            this.obiettivoComboEGroupSplitContainer.Panel1.Controls.Add(this.obiettiviLista);
            // 
            // obiettivoComboEGroupSplitContainer.Panel2
            // 
            this.obiettivoComboEGroupSplitContainer.Panel2.Controls.Add(this.obiettivoTableLayoutPanel);
            this.obiettivoComboEGroupSplitContainer.Size = new System.Drawing.Size(1066, 628);
            this.obiettivoComboEGroupSplitContainer.SplitterDistance = 48;
            this.obiettivoComboEGroupSplitContainer.SplitterWidth = 8;
            this.obiettivoComboEGroupSplitContainer.TabIndex = 7;
            // 
            // obiettiviLista
            // 
            this.obiettiviLista.Dock = System.Windows.Forms.DockStyle.Top;
            this.obiettiviLista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettiviLista.FormattingEnabled = true;
            this.obiettiviLista.Items.AddRange(new object[] {
            "assa",
            "gsg"});
            this.obiettiviLista.Location = new System.Drawing.Point(0, 0);
            this.obiettiviLista.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettiviLista.Name = "obiettiviLista";
            this.obiettiviLista.Size = new System.Drawing.Size(1066, 33);
            this.obiettiviLista.TabIndex = 0;
            this.obiettiviLista.SelectedIndexChanged += new System.EventHandler(this.ObiettiviLista_SelectedIndexChanged);
            // 
            // obiettivoTableLayoutPanel
            // 
            this.obiettivoTableLayoutPanel.AutoSize = true;
            this.obiettivoTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoTableLayoutPanel.ColumnCount = 2;
            this.obiettivoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoDescrizioneGroupBox, 0, 0);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoSceltaSottoObiettiviGroupBox, 1, 1);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoDurataGiorniGroupBox, 1, 0);
            this.obiettivoTableLayoutPanel.Controls.Add(this.obiettivoSceltaAttivitaGroupBox, 0, 1);
            this.obiettivoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.obiettivoTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoTableLayoutPanel.Name = "obiettivoTableLayoutPanel";
            this.obiettivoTableLayoutPanel.RowCount = 2;
            this.obiettivoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.obiettivoTableLayoutPanel.Size = new System.Drawing.Size(1066, 572);
            this.obiettivoTableLayoutPanel.TabIndex = 6;
            // 
            // obiettivoDescrizioneGroupBox
            // 
            this.obiettivoDescrizioneGroupBox.AutoSize = true;
            this.obiettivoDescrizioneGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoDescrizioneGroupBox.Controls.Add(this.obiettivoDescrizioneTextBox);
            this.obiettivoDescrizioneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoDescrizioneGroupBox.Location = new System.Drawing.Point(4, 4);
            this.obiettivoDescrizioneGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneGroupBox.Name = "obiettivoDescrizioneGroupBox";
            this.obiettivoDescrizioneGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneGroupBox.Size = new System.Drawing.Size(525, 278);
            this.obiettivoDescrizioneGroupBox.TabIndex = 1;
            this.obiettivoDescrizioneGroupBox.TabStop = false;
            this.obiettivoDescrizioneGroupBox.Text = "Descrizione";
            // 
            // obiettivoDescrizioneTextBox
            // 
            this.obiettivoDescrizioneTextBox.Location = new System.Drawing.Point(52, 90);
            this.obiettivoDescrizioneTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDescrizioneTextBox.Multiline = true;
            this.obiettivoDescrizioneTextBox.Name = "obiettivoDescrizioneTextBox";
            this.obiettivoDescrizioneTextBox.Size = new System.Drawing.Size(396, 141);
            this.obiettivoDescrizioneTextBox.TabIndex = 0;
            this.obiettivoDescrizioneTextBox.TextChanged += new System.EventHandler(this.ObiettivoDescrizioneTextBox_TextChanged);
            // 
            // obiettivoSceltaSottoObiettiviGroupBox
            // 
            this.obiettivoSceltaSottoObiettiviGroupBox.AutoSize = true;
            this.obiettivoSceltaSottoObiettiviGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSceltaSottoObiettiviGroupBox.Controls.Add(this.obiettivoAggiungiSottoObiettivoButton);
            this.obiettivoSceltaSottoObiettiviGroupBox.Controls.Add(this.obiettivoSceltaSottoObiettivoComboBox);
            this.obiettivoSceltaSottoObiettiviGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSceltaSottoObiettiviGroupBox.Location = new System.Drawing.Point(537, 290);
            this.obiettivoSceltaSottoObiettiviGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettiviGroupBox.Name = "obiettivoSceltaSottoObiettiviGroupBox";
            this.obiettivoSceltaSottoObiettiviGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettiviGroupBox.Size = new System.Drawing.Size(525, 278);
            this.obiettivoSceltaSottoObiettiviGroupBox.TabIndex = 3;
            this.obiettivoSceltaSottoObiettiviGroupBox.TabStop = false;
            this.obiettivoSceltaSottoObiettiviGroupBox.Text = "Sotto-obiettivo/i da completare";
            // 
            // obiettivoAggiungiSottoObiettivoButton
            // 
            this.obiettivoAggiungiSottoObiettivoButton.Enabled = false;
            this.obiettivoAggiungiSottoObiettivoButton.Location = new System.Drawing.Point(186, 171);
            this.obiettivoAggiungiSottoObiettivoButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAggiungiSottoObiettivoButton.Name = "obiettivoAggiungiSottoObiettivoButton";
            this.obiettivoAggiungiSottoObiettivoButton.Size = new System.Drawing.Size(184, 54);
            this.obiettivoAggiungiSottoObiettivoButton.TabIndex = 1;
            this.obiettivoAggiungiSottoObiettivoButton.Text = "Scegli obiettivo";
            this.obiettivoAggiungiSottoObiettivoButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiSottoObiettivoButton.Click += new System.EventHandler(this.ObiettivoAggiungiSottoObiettivoButton_Click);
            // 
            // obiettivoSceltaSottoObiettivoComboBox
            // 
            this.obiettivoSceltaSottoObiettivoComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettivoSceltaSottoObiettivoComboBox.FormattingEnabled = true;
            this.obiettivoSceltaSottoObiettivoComboBox.Items.AddRange(new object[] {
            "a",
            "b"});
            this.obiettivoSceltaSottoObiettivoComboBox.Location = new System.Drawing.Point(186, 98);
            this.obiettivoSceltaSottoObiettivoComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaSottoObiettivoComboBox.Name = "obiettivoSceltaSottoObiettivoComboBox";
            this.obiettivoSceltaSottoObiettivoComboBox.Size = new System.Drawing.Size(180, 33);
            this.obiettivoSceltaSottoObiettivoComboBox.TabIndex = 0;
            this.obiettivoSceltaSottoObiettivoComboBox.SelectedIndexChanged += new System.EventHandler(this.ObiettivoSceltaSottoObiettivoComboBox_SelectedIndexChanged);
            // 
            // obiettivoDurataGiorniGroupBox
            // 
            this.obiettivoDurataGiorniGroupBox.AutoSize = true;
            this.obiettivoDurataGiorniGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoAvviaButton);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoALabel);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoDaLabel);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoTermineDurataDateTimePicker);
            this.obiettivoDurataGiorniGroupBox.Controls.Add(this.obiettivoInizioDurataDateTimePicker);
            this.obiettivoDurataGiorniGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoDurataGiorniGroupBox.Location = new System.Drawing.Point(537, 4);
            this.obiettivoDurataGiorniGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDurataGiorniGroupBox.Name = "obiettivoDurataGiorniGroupBox";
            this.obiettivoDurataGiorniGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoDurataGiorniGroupBox.Size = new System.Drawing.Size(525, 278);
            this.obiettivoDurataGiorniGroupBox.TabIndex = 4;
            this.obiettivoDurataGiorniGroupBox.TabStop = false;
            this.obiettivoDurataGiorniGroupBox.Text = "Durata";
            // 
            // obiettivoAvviaButton
            // 
            this.obiettivoAvviaButton.Enabled = false;
            this.obiettivoAvviaButton.Location = new System.Drawing.Point(176, 208);
            this.obiettivoAvviaButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAvviaButton.Name = "obiettivoAvviaButton";
            this.obiettivoAvviaButton.Size = new System.Drawing.Size(200, 54);
            this.obiettivoAvviaButton.TabIndex = 4;
            this.obiettivoAvviaButton.Text = "Avvia";
            this.obiettivoAvviaButton.UseVisualStyleBackColor = true;
            this.obiettivoAvviaButton.Click += new System.EventHandler(this.ObiettivoAvviaButton_Click);
            // 
            // obiettivoALabel
            // 
            this.obiettivoALabel.AutoSize = true;
            this.obiettivoALabel.Location = new System.Drawing.Point(170, 115);
            this.obiettivoALabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoALabel.Name = "obiettivoALabel";
            this.obiettivoALabel.Size = new System.Drawing.Size(26, 25);
            this.obiettivoALabel.TabIndex = 3;
            this.obiettivoALabel.Text = "A";
            // 
            // obiettivoDaLabel
            // 
            this.obiettivoDaLabel.AutoSize = true;
            this.obiettivoDaLabel.Location = new System.Drawing.Point(170, 40);
            this.obiettivoDaLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoDaLabel.Name = "obiettivoDaLabel";
            this.obiettivoDaLabel.Size = new System.Drawing.Size(39, 25);
            this.obiettivoDaLabel.TabIndex = 2;
            this.obiettivoDaLabel.Text = "Da";
            // 
            // obiettivoTermineDurataDateTimePicker
            // 
            this.obiettivoTermineDurataDateTimePicker.Enabled = false;
            this.obiettivoTermineDurataDateTimePicker.Location = new System.Drawing.Point(176, 148);
            this.obiettivoTermineDurataDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoTermineDurataDateTimePicker.Name = "obiettivoTermineDurataDateTimePicker";
            this.obiettivoTermineDurataDateTimePicker.Size = new System.Drawing.Size(196, 31);
            this.obiettivoTermineDurataDateTimePicker.TabIndex = 1;
            // 
            // obiettivoInizioDurataDateTimePicker
            // 
            this.obiettivoInizioDurataDateTimePicker.Enabled = false;
            this.obiettivoInizioDurataDateTimePicker.Location = new System.Drawing.Point(176, 73);
            this.obiettivoInizioDurataDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoInizioDurataDateTimePicker.Name = "obiettivoInizioDurataDateTimePicker";
            this.obiettivoInizioDurataDateTimePicker.Size = new System.Drawing.Size(194, 31);
            this.obiettivoInizioDurataDateTimePicker.TabIndex = 0;
            // 
            // obiettivoSceltaAttivitaGroupBox
            // 
            this.obiettivoSceltaAttivitaGroupBox.AutoSize = true;
            this.obiettivoSceltaAttivitaGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoAggiungiAttivitaButton);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoNVolteTextBox);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoNVolteLabel);
            this.obiettivoSceltaAttivitaGroupBox.Controls.Add(this.obiettivoSceltaAttivitaComboBox);
            this.obiettivoSceltaAttivitaGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSceltaAttivitaGroupBox.Location = new System.Drawing.Point(4, 290);
            this.obiettivoSceltaAttivitaGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaGroupBox.Name = "obiettivoSceltaAttivitaGroupBox";
            this.obiettivoSceltaAttivitaGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaGroupBox.Size = new System.Drawing.Size(525, 278);
            this.obiettivoSceltaAttivitaGroupBox.TabIndex = 5;
            this.obiettivoSceltaAttivitaGroupBox.TabStop = false;
            this.obiettivoSceltaAttivitaGroupBox.Text = "Attività da completare (con quante volte)";
            // 
            // obiettivoAggiungiAttivitaButton
            // 
            this.obiettivoAggiungiAttivitaButton.Enabled = false;
            this.obiettivoAggiungiAttivitaButton.Location = new System.Drawing.Point(164, 187);
            this.obiettivoAggiungiAttivitaButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoAggiungiAttivitaButton.Name = "obiettivoAggiungiAttivitaButton";
            this.obiettivoAggiungiAttivitaButton.Size = new System.Drawing.Size(202, 46);
            this.obiettivoAggiungiAttivitaButton.TabIndex = 3;
            this.obiettivoAggiungiAttivitaButton.Text = "Scegli attività";
            this.obiettivoAggiungiAttivitaButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiAttivitaButton.Click += new System.EventHandler(this.ObiettivoAggiungiAttivitaButton_Click);
            // 
            // obiettivoNVolteTextBox
            // 
            this.obiettivoNVolteTextBox.Location = new System.Drawing.Point(164, 133);
            this.obiettivoNVolteTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoNVolteTextBox.Name = "obiettivoNVolteTextBox";
            this.obiettivoNVolteTextBox.Size = new System.Drawing.Size(196, 31);
            this.obiettivoNVolteTextBox.TabIndex = 2;
            this.obiettivoNVolteTextBox.TextChanged += new System.EventHandler(this.ObiettivoNVolteTextBox_TextChanged);
            // 
            // obiettivoNVolteLabel
            // 
            this.obiettivoNVolteLabel.AutoSize = true;
            this.obiettivoNVolteLabel.Location = new System.Drawing.Point(158, 104);
            this.obiettivoNVolteLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.obiettivoNVolteLabel.Name = "obiettivoNVolteLabel";
            this.obiettivoNVolteLabel.Size = new System.Drawing.Size(35, 25);
            this.obiettivoNVolteLabel.TabIndex = 1;
            this.obiettivoNVolteLabel.Text = "N°";
            // 
            // obiettivoSceltaAttivitaComboBox
            // 
            this.obiettivoSceltaAttivitaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.obiettivoSceltaAttivitaComboBox.FormattingEnabled = true;
            this.obiettivoSceltaAttivitaComboBox.Items.AddRange(new object[] {
            "uno",
            "due"});
            this.obiettivoSceltaAttivitaComboBox.Location = new System.Drawing.Point(164, 58);
            this.obiettivoSceltaAttivitaComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obiettivoSceltaAttivitaComboBox.Name = "obiettivoSceltaAttivitaComboBox";
            this.obiettivoSceltaAttivitaComboBox.Size = new System.Drawing.Size(196, 33);
            this.obiettivoSceltaAttivitaComboBox.TabIndex = 0;
            this.obiettivoSceltaAttivitaComboBox.SelectedIndexChanged += new System.EventHandler(this.ObiettivoSceltaAttivitaComboBox_SelectedIndexChanged);
            // 
            // obiettivoBottoniTableLayoutPanel
            // 
            this.obiettivoBottoniTableLayoutPanel.AutoSize = true;
            this.obiettivoBottoniTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoBottoniTableLayoutPanel.ColumnCount = 4;
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoSalvaButton, 1, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoEliminaButton, 0, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoAggiungiButton, 3, 0);
            this.obiettivoBottoniTableLayoutPanel.Controls.Add(this.obiettivoVediButton, 2, 0);
            this.obiettivoBottoniTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoBottoniTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.obiettivoBottoniTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoBottoniTableLayoutPanel.Name = "obiettivoBottoniTableLayoutPanel";
            this.obiettivoBottoniTableLayoutPanel.RowCount = 1;
            this.obiettivoBottoniTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.obiettivoBottoniTableLayoutPanel.Size = new System.Drawing.Size(1066, 116);
            this.obiettivoBottoniTableLayoutPanel.TabIndex = 3;
            // 
            // obiettivoSalvaButton
            // 
            this.obiettivoSalvaButton.AutoSize = true;
            this.obiettivoSalvaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoSalvaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoSalvaButton.Enabled = false;
            this.obiettivoSalvaButton.Location = new System.Drawing.Point(266, 0);
            this.obiettivoSalvaButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoSalvaButton.Name = "obiettivoSalvaButton";
            this.obiettivoSalvaButton.Size = new System.Drawing.Size(266, 116);
            this.obiettivoSalvaButton.TabIndex = 2;
            this.obiettivoSalvaButton.Text = "Salva obiettivo";
            this.obiettivoSalvaButton.UseVisualStyleBackColor = true;
            this.obiettivoSalvaButton.Click += new System.EventHandler(this.ObiettivoSalvaButton_Click);
            // 
            // obiettivoEliminaButton
            // 
            this.obiettivoEliminaButton.AutoSize = true;
            this.obiettivoEliminaButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoEliminaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoEliminaButton.Enabled = false;
            this.obiettivoEliminaButton.Location = new System.Drawing.Point(0, 0);
            this.obiettivoEliminaButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoEliminaButton.Name = "obiettivoEliminaButton";
            this.obiettivoEliminaButton.Size = new System.Drawing.Size(266, 116);
            this.obiettivoEliminaButton.TabIndex = 1;
            this.obiettivoEliminaButton.Text = "Elimina obiettivo";
            this.obiettivoEliminaButton.UseVisualStyleBackColor = true;
            this.obiettivoEliminaButton.Click += new System.EventHandler(this.ObiettivoEliminaButton_Click);
            // 
            // obiettivoAggiungiButton
            // 
            this.obiettivoAggiungiButton.AutoSize = true;
            this.obiettivoAggiungiButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoAggiungiButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoAggiungiButton.Enabled = false;
            this.obiettivoAggiungiButton.Location = new System.Drawing.Point(798, 0);
            this.obiettivoAggiungiButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoAggiungiButton.Name = "obiettivoAggiungiButton";
            this.obiettivoAggiungiButton.Size = new System.Drawing.Size(268, 116);
            this.obiettivoAggiungiButton.TabIndex = 0;
            this.obiettivoAggiungiButton.Text = "Aggiungi obiettivo";
            this.obiettivoAggiungiButton.UseVisualStyleBackColor = true;
            this.obiettivoAggiungiButton.Click += new System.EventHandler(this.ObiettivoAggiungiButton_Click);
            // 
            // obiettivoVediButton
            // 
            this.obiettivoVediButton.AutoSize = true;
            this.obiettivoVediButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.obiettivoVediButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.obiettivoVediButton.Location = new System.Drawing.Point(532, 0);
            this.obiettivoVediButton.Margin = new System.Windows.Forms.Padding(0);
            this.obiettivoVediButton.Name = "obiettivoVediButton";
            this.obiettivoVediButton.Size = new System.Drawing.Size(266, 116);
            this.obiettivoVediButton.TabIndex = 3;
            this.obiettivoVediButton.Text = "Vedi obiettivi";
            this.obiettivoVediButton.UseVisualStyleBackColor = true;
            this.obiettivoVediButton.Click += new System.EventHandler(this.ObiettivoVediButton_Click);
            // 
            // pannelloCalendario
            // 
            this.pannelloCalendario.AutoSize = true;
            this.pannelloCalendario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pannelloCalendario.ColumnCount = 1;
            this.pannelloCalendario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pannelloCalendario.Controls.Add(this.calendarioMensileTableLayoutPanel, 0, 1);
            this.pannelloCalendario.Controls.Add(this.calendarioMeseTableLayoutPanel, 0, 0);
            this.pannelloCalendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloCalendario.Location = new System.Drawing.Point(0, 0);
            this.pannelloCalendario.Margin = new System.Windows.Forms.Padding(0);
            this.pannelloCalendario.Name = "pannelloCalendario";
            this.pannelloCalendario.RowCount = 2;
            this.pannelloCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.74036F));
            this.pannelloCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.25964F));
            this.pannelloCalendario.Size = new System.Drawing.Size(1066, 748);
            this.pannelloCalendario.TabIndex = 3;
            // 
            // calendarioMensileTableLayoutPanel
            // 
            this.calendarioMensileTableLayoutPanel.AutoSize = true;
            this.calendarioMensileTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioMensileTableLayoutPanel.ColumnCount = 7;
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno7, 6, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno6, 5, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno5, 4, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno4, 3, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno3, 2, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno2, 1, 0);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno35, 6, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno34, 5, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno33, 4, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno32, 3, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno31, 2, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno30, 1, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno29, 0, 5);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno28, 6, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno27, 5, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno26, 4, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno25, 3, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno24, 2, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno23, 1, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno22, 0, 4);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno21, 6, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno20, 5, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno19, 4, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno18, 3, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno17, 2, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno16, 1, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno15, 0, 3);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno14, 6, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno13, 5, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno12, 4, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno11, 3, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno10, 2, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno9, 1, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno8, 0, 2);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno7, 6, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno6, 5, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno5, 4, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno4, 3, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno3, 2, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno2, 1, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioBottoneGiorno1, 0, 1);
            this.calendarioMensileTableLayoutPanel.Controls.Add(this.calendarioMeseGiorno1, 0, 0);
            this.calendarioMensileTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMensileTableLayoutPanel.Location = new System.Drawing.Point(0, 65);
            this.calendarioMensileTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMensileTableLayoutPanel.Name = "calendarioMensileTableLayoutPanel";
            this.calendarioMensileTableLayoutPanel.RowCount = 6;
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.38356F));
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.12329F));
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.12329F));
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.12329F));
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.12329F));
            this.calendarioMensileTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.12329F));
            this.calendarioMensileTableLayoutPanel.Size = new System.Drawing.Size(1066, 683);
            this.calendarioMensileTableLayoutPanel.TabIndex = 1;
            // 
            // calendarioMeseGiorno7
            // 
            this.calendarioMeseGiorno7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno7.Enabled = false;
            this.calendarioMeseGiorno7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno7.Location = new System.Drawing.Point(912, 0);
            this.calendarioMeseGiorno7.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno7.Multiline = true;
            this.calendarioMeseGiorno7.Name = "calendarioMeseGiorno7";
            this.calendarioMeseGiorno7.Size = new System.Drawing.Size(154, 98);
            this.calendarioMeseGiorno7.TabIndex = 46;
            // 
            // calendarioMeseGiorno6
            // 
            this.calendarioMeseGiorno6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno6.Enabled = false;
            this.calendarioMeseGiorno6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno6.Location = new System.Drawing.Point(760, 0);
            this.calendarioMeseGiorno6.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno6.Multiline = true;
            this.calendarioMeseGiorno6.Name = "calendarioMeseGiorno6";
            this.calendarioMeseGiorno6.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno6.TabIndex = 45;
            // 
            // calendarioMeseGiorno5
            // 
            this.calendarioMeseGiorno5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno5.Enabled = false;
            this.calendarioMeseGiorno5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno5.Location = new System.Drawing.Point(608, 0);
            this.calendarioMeseGiorno5.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno5.Multiline = true;
            this.calendarioMeseGiorno5.Name = "calendarioMeseGiorno5";
            this.calendarioMeseGiorno5.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno5.TabIndex = 44;
            // 
            // calendarioMeseGiorno4
            // 
            this.calendarioMeseGiorno4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno4.Enabled = false;
            this.calendarioMeseGiorno4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno4.Location = new System.Drawing.Point(456, 0);
            this.calendarioMeseGiorno4.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno4.Multiline = true;
            this.calendarioMeseGiorno4.Name = "calendarioMeseGiorno4";
            this.calendarioMeseGiorno4.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno4.TabIndex = 43;
            // 
            // calendarioMeseGiorno3
            // 
            this.calendarioMeseGiorno3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno3.Enabled = false;
            this.calendarioMeseGiorno3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno3.Location = new System.Drawing.Point(304, 0);
            this.calendarioMeseGiorno3.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno3.Multiline = true;
            this.calendarioMeseGiorno3.Name = "calendarioMeseGiorno3";
            this.calendarioMeseGiorno3.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno3.TabIndex = 42;
            // 
            // calendarioMeseGiorno2
            // 
            this.calendarioMeseGiorno2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno2.Enabled = false;
            this.calendarioMeseGiorno2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno2.Location = new System.Drawing.Point(152, 0);
            this.calendarioMeseGiorno2.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno2.Multiline = true;
            this.calendarioMeseGiorno2.Name = "calendarioMeseGiorno2";
            this.calendarioMeseGiorno2.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno2.TabIndex = 41;
            // 
            // calendarioBottoneGiorno35
            // 
            this.calendarioBottoneGiorno35.AutoSize = true;
            this.calendarioBottoneGiorno35.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno35.Location = new System.Drawing.Point(912, 562);
            this.calendarioBottoneGiorno35.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno35.Name = "calendarioBottoneGiorno35";
            this.calendarioBottoneGiorno35.Size = new System.Drawing.Size(154, 121);
            this.calendarioBottoneGiorno35.TabIndex = 39;
            this.calendarioBottoneGiorno35.Text = "1";
            this.calendarioBottoneGiorno35.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno34
            // 
            this.calendarioBottoneGiorno34.AutoSize = true;
            this.calendarioBottoneGiorno34.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno34.Location = new System.Drawing.Point(760, 562);
            this.calendarioBottoneGiorno34.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno34.Name = "calendarioBottoneGiorno34";
            this.calendarioBottoneGiorno34.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno34.TabIndex = 38;
            this.calendarioBottoneGiorno34.Text = "1";
            this.calendarioBottoneGiorno34.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno33
            // 
            this.calendarioBottoneGiorno33.AutoSize = true;
            this.calendarioBottoneGiorno33.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno33.Location = new System.Drawing.Point(608, 562);
            this.calendarioBottoneGiorno33.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno33.Name = "calendarioBottoneGiorno33";
            this.calendarioBottoneGiorno33.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno33.TabIndex = 37;
            this.calendarioBottoneGiorno33.Text = "1";
            this.calendarioBottoneGiorno33.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno32
            // 
            this.calendarioBottoneGiorno32.AutoSize = true;
            this.calendarioBottoneGiorno32.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno32.Location = new System.Drawing.Point(456, 562);
            this.calendarioBottoneGiorno32.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno32.Name = "calendarioBottoneGiorno32";
            this.calendarioBottoneGiorno32.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno32.TabIndex = 36;
            this.calendarioBottoneGiorno32.Text = "1";
            this.calendarioBottoneGiorno32.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno31
            // 
            this.calendarioBottoneGiorno31.AutoSize = true;
            this.calendarioBottoneGiorno31.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno31.Location = new System.Drawing.Point(304, 562);
            this.calendarioBottoneGiorno31.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno31.Name = "calendarioBottoneGiorno31";
            this.calendarioBottoneGiorno31.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno31.TabIndex = 35;
            this.calendarioBottoneGiorno31.Text = "1";
            this.calendarioBottoneGiorno31.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno30
            // 
            this.calendarioBottoneGiorno30.AutoSize = true;
            this.calendarioBottoneGiorno30.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno30.Location = new System.Drawing.Point(152, 562);
            this.calendarioBottoneGiorno30.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno30.Name = "calendarioBottoneGiorno30";
            this.calendarioBottoneGiorno30.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno30.TabIndex = 34;
            this.calendarioBottoneGiorno30.Text = "1";
            this.calendarioBottoneGiorno30.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno29
            // 
            this.calendarioBottoneGiorno29.AutoSize = true;
            this.calendarioBottoneGiorno29.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno29.Location = new System.Drawing.Point(0, 562);
            this.calendarioBottoneGiorno29.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno29.Name = "calendarioBottoneGiorno29";
            this.calendarioBottoneGiorno29.Size = new System.Drawing.Size(152, 121);
            this.calendarioBottoneGiorno29.TabIndex = 33;
            this.calendarioBottoneGiorno29.Text = "1";
            this.calendarioBottoneGiorno29.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno28
            // 
            this.calendarioBottoneGiorno28.AutoSize = true;
            this.calendarioBottoneGiorno28.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno28.Location = new System.Drawing.Point(912, 446);
            this.calendarioBottoneGiorno28.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno28.Name = "calendarioBottoneGiorno28";
            this.calendarioBottoneGiorno28.Size = new System.Drawing.Size(154, 116);
            this.calendarioBottoneGiorno28.TabIndex = 32;
            this.calendarioBottoneGiorno28.Text = "1";
            this.calendarioBottoneGiorno28.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno27
            // 
            this.calendarioBottoneGiorno27.AutoSize = true;
            this.calendarioBottoneGiorno27.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno27.Location = new System.Drawing.Point(760, 446);
            this.calendarioBottoneGiorno27.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno27.Name = "calendarioBottoneGiorno27";
            this.calendarioBottoneGiorno27.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno27.TabIndex = 31;
            this.calendarioBottoneGiorno27.Text = "1";
            this.calendarioBottoneGiorno27.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno26
            // 
            this.calendarioBottoneGiorno26.AutoSize = true;
            this.calendarioBottoneGiorno26.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno26.Location = new System.Drawing.Point(608, 446);
            this.calendarioBottoneGiorno26.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno26.Name = "calendarioBottoneGiorno26";
            this.calendarioBottoneGiorno26.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno26.TabIndex = 30;
            this.calendarioBottoneGiorno26.Text = "1";
            this.calendarioBottoneGiorno26.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno25
            // 
            this.calendarioBottoneGiorno25.AutoSize = true;
            this.calendarioBottoneGiorno25.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno25.Location = new System.Drawing.Point(456, 446);
            this.calendarioBottoneGiorno25.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno25.Name = "calendarioBottoneGiorno25";
            this.calendarioBottoneGiorno25.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno25.TabIndex = 29;
            this.calendarioBottoneGiorno25.Text = "1";
            this.calendarioBottoneGiorno25.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno24
            // 
            this.calendarioBottoneGiorno24.AutoSize = true;
            this.calendarioBottoneGiorno24.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno24.Location = new System.Drawing.Point(304, 446);
            this.calendarioBottoneGiorno24.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno24.Name = "calendarioBottoneGiorno24";
            this.calendarioBottoneGiorno24.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno24.TabIndex = 28;
            this.calendarioBottoneGiorno24.Text = "1";
            this.calendarioBottoneGiorno24.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno23
            // 
            this.calendarioBottoneGiorno23.AutoSize = true;
            this.calendarioBottoneGiorno23.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno23.Location = new System.Drawing.Point(152, 446);
            this.calendarioBottoneGiorno23.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno23.Name = "calendarioBottoneGiorno23";
            this.calendarioBottoneGiorno23.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno23.TabIndex = 27;
            this.calendarioBottoneGiorno23.Text = "1";
            this.calendarioBottoneGiorno23.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno22
            // 
            this.calendarioBottoneGiorno22.AutoSize = true;
            this.calendarioBottoneGiorno22.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno22.Location = new System.Drawing.Point(0, 446);
            this.calendarioBottoneGiorno22.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno22.Name = "calendarioBottoneGiorno22";
            this.calendarioBottoneGiorno22.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno22.TabIndex = 26;
            this.calendarioBottoneGiorno22.Text = "1";
            this.calendarioBottoneGiorno22.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno21
            // 
            this.calendarioBottoneGiorno21.AutoSize = true;
            this.calendarioBottoneGiorno21.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno21.Location = new System.Drawing.Point(912, 330);
            this.calendarioBottoneGiorno21.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno21.Name = "calendarioBottoneGiorno21";
            this.calendarioBottoneGiorno21.Size = new System.Drawing.Size(154, 116);
            this.calendarioBottoneGiorno21.TabIndex = 25;
            this.calendarioBottoneGiorno21.Text = "1";
            this.calendarioBottoneGiorno21.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno20
            // 
            this.calendarioBottoneGiorno20.AutoSize = true;
            this.calendarioBottoneGiorno20.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno20.Location = new System.Drawing.Point(760, 330);
            this.calendarioBottoneGiorno20.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno20.Name = "calendarioBottoneGiorno20";
            this.calendarioBottoneGiorno20.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno20.TabIndex = 24;
            this.calendarioBottoneGiorno20.Text = "1";
            this.calendarioBottoneGiorno20.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno19
            // 
            this.calendarioBottoneGiorno19.AutoSize = true;
            this.calendarioBottoneGiorno19.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno19.Location = new System.Drawing.Point(608, 330);
            this.calendarioBottoneGiorno19.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno19.Name = "calendarioBottoneGiorno19";
            this.calendarioBottoneGiorno19.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno19.TabIndex = 23;
            this.calendarioBottoneGiorno19.Text = "1";
            this.calendarioBottoneGiorno19.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno18
            // 
            this.calendarioBottoneGiorno18.AutoSize = true;
            this.calendarioBottoneGiorno18.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno18.Location = new System.Drawing.Point(456, 330);
            this.calendarioBottoneGiorno18.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno18.Name = "calendarioBottoneGiorno18";
            this.calendarioBottoneGiorno18.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno18.TabIndex = 22;
            this.calendarioBottoneGiorno18.Text = "1";
            this.calendarioBottoneGiorno18.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno17
            // 
            this.calendarioBottoneGiorno17.AutoSize = true;
            this.calendarioBottoneGiorno17.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno17.Location = new System.Drawing.Point(304, 330);
            this.calendarioBottoneGiorno17.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno17.Name = "calendarioBottoneGiorno17";
            this.calendarioBottoneGiorno17.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno17.TabIndex = 21;
            this.calendarioBottoneGiorno17.Text = "1";
            this.calendarioBottoneGiorno17.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno16
            // 
            this.calendarioBottoneGiorno16.AutoSize = true;
            this.calendarioBottoneGiorno16.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno16.Location = new System.Drawing.Point(152, 330);
            this.calendarioBottoneGiorno16.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno16.Name = "calendarioBottoneGiorno16";
            this.calendarioBottoneGiorno16.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno16.TabIndex = 20;
            this.calendarioBottoneGiorno16.Text = "1";
            this.calendarioBottoneGiorno16.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno15
            // 
            this.calendarioBottoneGiorno15.AutoSize = true;
            this.calendarioBottoneGiorno15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno15.Location = new System.Drawing.Point(0, 330);
            this.calendarioBottoneGiorno15.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno15.Name = "calendarioBottoneGiorno15";
            this.calendarioBottoneGiorno15.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno15.TabIndex = 19;
            this.calendarioBottoneGiorno15.Text = "1";
            this.calendarioBottoneGiorno15.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno14
            // 
            this.calendarioBottoneGiorno14.AutoSize = true;
            this.calendarioBottoneGiorno14.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno14.Location = new System.Drawing.Point(912, 214);
            this.calendarioBottoneGiorno14.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno14.Name = "calendarioBottoneGiorno14";
            this.calendarioBottoneGiorno14.Size = new System.Drawing.Size(154, 116);
            this.calendarioBottoneGiorno14.TabIndex = 18;
            this.calendarioBottoneGiorno14.Text = "1";
            this.calendarioBottoneGiorno14.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno13
            // 
            this.calendarioBottoneGiorno13.AutoSize = true;
            this.calendarioBottoneGiorno13.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno13.Location = new System.Drawing.Point(760, 214);
            this.calendarioBottoneGiorno13.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno13.Name = "calendarioBottoneGiorno13";
            this.calendarioBottoneGiorno13.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno13.TabIndex = 17;
            this.calendarioBottoneGiorno13.Text = "1";
            this.calendarioBottoneGiorno13.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno12
            // 
            this.calendarioBottoneGiorno12.AutoSize = true;
            this.calendarioBottoneGiorno12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno12.Location = new System.Drawing.Point(608, 214);
            this.calendarioBottoneGiorno12.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno12.Name = "calendarioBottoneGiorno12";
            this.calendarioBottoneGiorno12.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno12.TabIndex = 16;
            this.calendarioBottoneGiorno12.Text = "1";
            this.calendarioBottoneGiorno12.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno11
            // 
            this.calendarioBottoneGiorno11.AutoSize = true;
            this.calendarioBottoneGiorno11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno11.Location = new System.Drawing.Point(456, 214);
            this.calendarioBottoneGiorno11.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno11.Name = "calendarioBottoneGiorno11";
            this.calendarioBottoneGiorno11.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno11.TabIndex = 15;
            this.calendarioBottoneGiorno11.Text = "1";
            this.calendarioBottoneGiorno11.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno10
            // 
            this.calendarioBottoneGiorno10.AutoSize = true;
            this.calendarioBottoneGiorno10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno10.Location = new System.Drawing.Point(304, 214);
            this.calendarioBottoneGiorno10.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno10.Name = "calendarioBottoneGiorno10";
            this.calendarioBottoneGiorno10.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno10.TabIndex = 14;
            this.calendarioBottoneGiorno10.Text = "1";
            this.calendarioBottoneGiorno10.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno9
            // 
            this.calendarioBottoneGiorno9.AutoSize = true;
            this.calendarioBottoneGiorno9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno9.Location = new System.Drawing.Point(152, 214);
            this.calendarioBottoneGiorno9.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno9.Name = "calendarioBottoneGiorno9";
            this.calendarioBottoneGiorno9.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno9.TabIndex = 13;
            this.calendarioBottoneGiorno9.Text = "1";
            this.calendarioBottoneGiorno9.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno8
            // 
            this.calendarioBottoneGiorno8.AutoSize = true;
            this.calendarioBottoneGiorno8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno8.Location = new System.Drawing.Point(0, 214);
            this.calendarioBottoneGiorno8.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno8.Name = "calendarioBottoneGiorno8";
            this.calendarioBottoneGiorno8.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno8.TabIndex = 12;
            this.calendarioBottoneGiorno8.Text = "1";
            this.calendarioBottoneGiorno8.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno7
            // 
            this.calendarioBottoneGiorno7.AutoSize = true;
            this.calendarioBottoneGiorno7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno7.Location = new System.Drawing.Point(912, 98);
            this.calendarioBottoneGiorno7.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno7.Name = "calendarioBottoneGiorno7";
            this.calendarioBottoneGiorno7.Size = new System.Drawing.Size(154, 116);
            this.calendarioBottoneGiorno7.TabIndex = 11;
            this.calendarioBottoneGiorno7.Text = "1";
            this.calendarioBottoneGiorno7.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno6
            // 
            this.calendarioBottoneGiorno6.AutoSize = true;
            this.calendarioBottoneGiorno6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioBottoneGiorno6.Location = new System.Drawing.Point(760, 98);
            this.calendarioBottoneGiorno6.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno6.Name = "calendarioBottoneGiorno6";
            this.calendarioBottoneGiorno6.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno6.TabIndex = 10;
            this.calendarioBottoneGiorno6.Text = "1";
            this.calendarioBottoneGiorno6.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno5
            // 
            this.calendarioBottoneGiorno5.AutoSize = true;
            this.calendarioBottoneGiorno5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno5.Location = new System.Drawing.Point(608, 98);
            this.calendarioBottoneGiorno5.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno5.Name = "calendarioBottoneGiorno5";
            this.calendarioBottoneGiorno5.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno5.TabIndex = 9;
            this.calendarioBottoneGiorno5.Text = "1";
            this.calendarioBottoneGiorno5.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno4
            // 
            this.calendarioBottoneGiorno4.AutoSize = true;
            this.calendarioBottoneGiorno4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno4.Location = new System.Drawing.Point(456, 98);
            this.calendarioBottoneGiorno4.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno4.Name = "calendarioBottoneGiorno4";
            this.calendarioBottoneGiorno4.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno4.TabIndex = 8;
            this.calendarioBottoneGiorno4.Text = "1";
            this.calendarioBottoneGiorno4.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno3
            // 
            this.calendarioBottoneGiorno3.AutoSize = true;
            this.calendarioBottoneGiorno3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno3.Location = new System.Drawing.Point(304, 98);
            this.calendarioBottoneGiorno3.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno3.Name = "calendarioBottoneGiorno3";
            this.calendarioBottoneGiorno3.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno3.TabIndex = 7;
            this.calendarioBottoneGiorno3.Text = "1";
            this.calendarioBottoneGiorno3.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno2
            // 
            this.calendarioBottoneGiorno2.AutoSize = true;
            this.calendarioBottoneGiorno2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno2.Location = new System.Drawing.Point(152, 98);
            this.calendarioBottoneGiorno2.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno2.Name = "calendarioBottoneGiorno2";
            this.calendarioBottoneGiorno2.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno2.TabIndex = 6;
            this.calendarioBottoneGiorno2.Text = "1";
            this.calendarioBottoneGiorno2.UseVisualStyleBackColor = true;
            // 
            // calendarioBottoneGiorno1
            // 
            this.calendarioBottoneGiorno1.AutoSize = true;
            this.calendarioBottoneGiorno1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioBottoneGiorno1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioBottoneGiorno1.Location = new System.Drawing.Point(0, 98);
            this.calendarioBottoneGiorno1.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioBottoneGiorno1.Name = "calendarioBottoneGiorno1";
            this.calendarioBottoneGiorno1.Size = new System.Drawing.Size(152, 116);
            this.calendarioBottoneGiorno1.TabIndex = 5;
            this.calendarioBottoneGiorno1.Text = "1";
            this.calendarioBottoneGiorno1.UseVisualStyleBackColor = true;
            // 
            // calendarioMeseGiorno1
            // 
            this.calendarioMeseGiorno1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseGiorno1.Enabled = false;
            this.calendarioMeseGiorno1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseGiorno1.Location = new System.Drawing.Point(0, 0);
            this.calendarioMeseGiorno1.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseGiorno1.Multiline = true;
            this.calendarioMeseGiorno1.Name = "calendarioMeseGiorno1";
            this.calendarioMeseGiorno1.Size = new System.Drawing.Size(152, 98);
            this.calendarioMeseGiorno1.TabIndex = 40;
            // 
            // calendarioMeseTableLayoutPanel
            // 
            this.calendarioMeseTableLayoutPanel.AutoSize = true;
            this.calendarioMeseTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioMeseTableLayoutPanel.ColumnCount = 3;
            this.calendarioMeseTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.315589F));
            this.calendarioMeseTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.60837F));
            this.calendarioMeseTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.26616F));
            this.calendarioMeseTableLayoutPanel.Controls.Add(this.calendarioMeseTextBox, 1, 0);
            this.calendarioMeseTableLayoutPanel.Controls.Add(this.calendarioMeseBottoneSinistra, 0, 0);
            this.calendarioMeseTableLayoutPanel.Controls.Add(this.calendarioMeseFrecciaDestra, 2, 0);
            this.calendarioMeseTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.calendarioMeseTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseTableLayoutPanel.Name = "calendarioMeseTableLayoutPanel";
            this.calendarioMeseTableLayoutPanel.RowCount = 1;
            this.calendarioMeseTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.calendarioMeseTableLayoutPanel.Size = new System.Drawing.Size(1066, 65);
            this.calendarioMeseTableLayoutPanel.TabIndex = 3;
            // 
            // calendarioMeseTextBox
            // 
            this.calendarioMeseTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseTextBox.Enabled = false;
            this.calendarioMeseTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseTextBox.Location = new System.Drawing.Point(99, 0);
            this.calendarioMeseTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseTextBox.Multiline = true;
            this.calendarioMeseTextBox.Name = "calendarioMeseTextBox";
            this.calendarioMeseTextBox.Size = new System.Drawing.Size(857, 65);
            this.calendarioMeseTextBox.TabIndex = 2;
            this.calendarioMeseTextBox.Text = "Mese";
            this.calendarioMeseTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // calendarioMeseBottoneSinistra
            // 
            this.calendarioMeseBottoneSinistra.AutoSize = true;
            this.calendarioMeseBottoneSinistra.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioMeseBottoneSinistra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseBottoneSinistra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseBottoneSinistra.Location = new System.Drawing.Point(0, 0);
            this.calendarioMeseBottoneSinistra.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseBottoneSinistra.Name = "calendarioMeseBottoneSinistra";
            this.calendarioMeseBottoneSinistra.Size = new System.Drawing.Size(99, 65);
            this.calendarioMeseBottoneSinistra.TabIndex = 3;
            this.calendarioMeseBottoneSinistra.Text = "<-";
            this.calendarioMeseBottoneSinistra.UseVisualStyleBackColor = true;
            this.calendarioMeseBottoneSinistra.Click += new System.EventHandler(this.CalendarioMeseBottoneSinistra_Click);
            // 
            // calendarioMeseFrecciaDestra
            // 
            this.calendarioMeseFrecciaDestra.AutoSize = true;
            this.calendarioMeseFrecciaDestra.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calendarioMeseFrecciaDestra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioMeseFrecciaDestra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calendarioMeseFrecciaDestra.Location = new System.Drawing.Point(956, 0);
            this.calendarioMeseFrecciaDestra.Margin = new System.Windows.Forms.Padding(0);
            this.calendarioMeseFrecciaDestra.Name = "calendarioMeseFrecciaDestra";
            this.calendarioMeseFrecciaDestra.Size = new System.Drawing.Size(110, 65);
            this.calendarioMeseFrecciaDestra.TabIndex = 4;
            this.calendarioMeseFrecciaDestra.Text = "->";
            this.calendarioMeseFrecciaDestra.UseVisualStyleBackColor = true;
            this.calendarioMeseFrecciaDestra.Click += new System.EventHandler(this.CalendarioMeseFrecciaDestra_Click);
            // 
            // pannelloAchievement
            // 
            this.pannelloAchievement.AutoSize = true;
            this.pannelloAchievement.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pannelloAchievement.Controls.Add(this.achievementSplitContainer);
            this.pannelloAchievement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pannelloAchievement.Location = new System.Drawing.Point(0, 0);
            this.pannelloAchievement.Margin = new System.Windows.Forms.Padding(0);
            this.pannelloAchievement.Name = "pannelloAchievement";
            this.pannelloAchievement.Size = new System.Drawing.Size(1066, 748);
            this.pannelloAchievement.TabIndex = 7;
            // 
            // achievementSplitContainer
            // 
            this.achievementSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.achievementSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.achievementSplitContainer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementSplitContainer.Name = "achievementSplitContainer";
            // 
            // achievementSplitContainer.Panel1
            // 
            this.achievementSplitContainer.Panel1.Controls.Add(this.achievementListBox);
            // 
            // achievementSplitContainer.Panel2
            // 
            this.achievementSplitContainer.Panel2.Controls.Add(this.achievementDescrizioneGroupBox);
            this.achievementSplitContainer.Panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementSplitContainer.Size = new System.Drawing.Size(1066, 748);
            this.achievementSplitContainer.SplitterDistance = 508;
            this.achievementSplitContainer.TabIndex = 6;
            // 
            // achievementListBox
            // 
            this.achievementListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.achievementListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.achievementListBox.FormattingEnabled = true;
            this.achievementListBox.ItemHeight = 37;
            this.achievementListBox.Items.AddRange(new object[] {
            "achievement 1",
            "2"});
            this.achievementListBox.Location = new System.Drawing.Point(0, 0);
            this.achievementListBox.Margin = new System.Windows.Forms.Padding(0);
            this.achievementListBox.Name = "achievementListBox";
            this.achievementListBox.Size = new System.Drawing.Size(508, 748);
            this.achievementListBox.TabIndex = 0;
            this.achievementListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AchievementListBox_MouseDoubleClick);
            // 
            // achievementDescrizioneGroupBox
            // 
            this.achievementDescrizioneGroupBox.AutoSize = true;
            this.achievementDescrizioneGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementCompletamentoTextBox);
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementCompletamentoLabel);
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementDescrizioneTextBox);
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementDescrizioneLabel);
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementNomeTextBox);
            this.achievementDescrizioneGroupBox.Controls.Add(this.achievementNomeLabel);
            this.achievementDescrizioneGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.achievementDescrizioneGroupBox.Location = new System.Drawing.Point(0, 0);
            this.achievementDescrizioneGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementDescrizioneGroupBox.Name = "achievementDescrizioneGroupBox";
            this.achievementDescrizioneGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementDescrizioneGroupBox.Size = new System.Drawing.Size(554, 748);
            this.achievementDescrizioneGroupBox.TabIndex = 0;
            this.achievementDescrizioneGroupBox.TabStop = false;
            this.achievementDescrizioneGroupBox.Text = "Info";
            // 
            // achievementCompletamentoTextBox
            // 
            this.achievementCompletamentoTextBox.Enabled = false;
            this.achievementCompletamentoTextBox.Location = new System.Drawing.Point(12, 298);
            this.achievementCompletamentoTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementCompletamentoTextBox.Name = "achievementCompletamentoTextBox";
            this.achievementCompletamentoTextBox.Size = new System.Drawing.Size(214, 31);
            this.achievementCompletamentoTextBox.TabIndex = 5;
            // 
            // achievementCompletamentoLabel
            // 
            this.achievementCompletamentoLabel.AutoSize = true;
            this.achievementCompletamentoLabel.Location = new System.Drawing.Point(10, 265);
            this.achievementCompletamentoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.achievementCompletamentoLabel.Name = "achievementCompletamentoLabel";
            this.achievementCompletamentoLabel.Size = new System.Drawing.Size(279, 25);
            this.achievementCompletamentoLabel.TabIndex = 4;
            this.achievementCompletamentoLabel.Text = "Percentuale completamento";
            // 
            // achievementDescrizioneTextBox
            // 
            this.achievementDescrizioneTextBox.Enabled = false;
            this.achievementDescrizioneTextBox.Location = new System.Drawing.Point(12, 162);
            this.achievementDescrizioneTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementDescrizioneTextBox.Multiline = true;
            this.achievementDescrizioneTextBox.Name = "achievementDescrizioneTextBox";
            this.achievementDescrizioneTextBox.Size = new System.Drawing.Size(214, 71);
            this.achievementDescrizioneTextBox.TabIndex = 3;
            // 
            // achievementDescrizioneLabel
            // 
            this.achievementDescrizioneLabel.AutoSize = true;
            this.achievementDescrizioneLabel.Location = new System.Drawing.Point(10, 125);
            this.achievementDescrizioneLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.achievementDescrizioneLabel.Name = "achievementDescrizioneLabel";
            this.achievementDescrizioneLabel.Size = new System.Drawing.Size(125, 25);
            this.achievementDescrizioneLabel.TabIndex = 2;
            this.achievementDescrizioneLabel.Text = "Descrizione";
            // 
            // achievementNomeTextBox
            // 
            this.achievementNomeTextBox.Enabled = false;
            this.achievementNomeTextBox.Location = new System.Drawing.Point(12, 73);
            this.achievementNomeTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.achievementNomeTextBox.Name = "achievementNomeTextBox";
            this.achievementNomeTextBox.Size = new System.Drawing.Size(216, 31);
            this.achievementNomeTextBox.TabIndex = 1;
            // 
            // achievementNomeLabel
            // 
            this.achievementNomeLabel.AutoSize = true;
            this.achievementNomeLabel.Location = new System.Drawing.Point(8, 40);
            this.achievementNomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.achievementNomeLabel.Name = "achievementNomeLabel";
            this.achievementNomeLabel.Size = new System.Drawing.Size(68, 25);
            this.achievementNomeLabel.TabIndex = 0;
            this.achievementNomeLabel.Text = "Nome";
            // 
            // groupBox8
            // 
            this.groupBox8.AutoSize = true;
            this.groupBox8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(6, 864);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox8.Size = new System.Drawing.Size(1484, 178);
            this.groupBox8.TabIndex = 17;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Descrizione";
            // 
            // UserMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 748);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "UserMenu";
            this.Text = "UserMenu";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.listaBottoniSinistraTableLayoutPanel.ResumeLayout(false);
            this.listaBottoniSinistraTableLayoutPanel.PerformLayout();
            this.calendarioGiornalieroTableLayoutPanel.ResumeLayout(false);
            this.calendarioGiornalieroTableLayoutPanel.PerformLayout();
            this.calendarioAttivitaTableLayoutPanel.ResumeLayout(false);
            this.pannelloBudget.ResumeLayout(false);
            this.pannelloBudget.PerformLayout();
            this.budgetSpeseSplitContainer.Panel1.ResumeLayout(false);
            this.budgetSpeseSplitContainer.Panel1.PerformLayout();
            this.budgetSpeseSplitContainer.Panel2.ResumeLayout(false);
            this.budgetSpeseSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.budgetSpeseSplitContainer)).EndInit();
            this.budgetSpeseSplitContainer.ResumeLayout(false);
            this.budgetSpeseGroupBox.ResumeLayout(false);
            this.budgetSpeseGroupBox.PerformLayout();
            this.budgetBottoniTableLayoutPanel.ResumeLayout(false);
            this.budgetBottoniTableLayoutPanel.PerformLayout();
            this.budgetBudgetGroupBox.ResumeLayout(false);
            this.budgetBudgetGroupBox.PerformLayout();
            this.pannelloStatistiche.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.statisticheSplitContainer.Panel1.ResumeLayout(false);
            this.statisticheSplitContainer.Panel2.ResumeLayout(false);
            this.statisticheSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statisticheSplitContainer)).EndInit();
            this.statisticheSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graficoStatistiche)).EndInit();
            this.pannelloAttivita.ResumeLayout(false);
            this.SchermInizAttività.Panel1.ResumeLayout(false);
            this.SchermInizAttività.Panel2.ResumeLayout(false);
            this.SchermInizAttività.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchermInizAttività)).EndInit();
            this.SchermInizAttività.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.attivita_groupBoxDescrizione.ResumeLayout(false);
            this.attivita_groupBoxDescrizione.PerformLayout();
            this.attivita_groupBoxNome.ResumeLayout(false);
            this.attivita_groupBoxNome.PerformLayout();
            this.attivita_groupBoxColore.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.attivita_groupBoxPenitenza.ResumeLayout(false);
            this.attivita_groupBoxPenitenza.PerformLayout();
            this.attivita_groupBoxPremio.ResumeLayout(false);
            this.attivita_groupBoxPremio.PerformLayout();
            this.attivita_groupBoxCategoria.ResumeLayout(false);
            this.attivita_groupBoxCategoria.PerformLayout();
            this.durataAttivitaGroupBox.ResumeLayout(false);
            this.durataAttivitaGroupBox.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.obiettivo_pannello_iniziale.ResumeLayout(false);
            this.obiettivo_split_vista.Panel1.ResumeLayout(false);
            this.obiettivo_split_vista.Panel2.ResumeLayout(false);
            this.obiettivo_split_vista.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivo_split_vista)).EndInit();
            this.obiettivo_split_vista.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel1.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel2.ResumeLayout(false);
            this.obiettivoComboEGroupSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.obiettivoComboEGroupSplitContainer)).EndInit();
            this.obiettivoComboEGroupSplitContainer.ResumeLayout(false);
            this.obiettivoTableLayoutPanel.ResumeLayout(false);
            this.obiettivoTableLayoutPanel.PerformLayout();
            this.obiettivoDescrizioneGroupBox.ResumeLayout(false);
            this.obiettivoDescrizioneGroupBox.PerformLayout();
            this.obiettivoSceltaSottoObiettiviGroupBox.ResumeLayout(false);
            this.obiettivoDurataGiorniGroupBox.ResumeLayout(false);
            this.obiettivoDurataGiorniGroupBox.PerformLayout();
            this.obiettivoSceltaAttivitaGroupBox.ResumeLayout(false);
            this.obiettivoSceltaAttivitaGroupBox.PerformLayout();
            this.obiettivoBottoniTableLayoutPanel.ResumeLayout(false);
            this.obiettivoBottoniTableLayoutPanel.PerformLayout();
            this.pannelloCalendario.ResumeLayout(false);
            this.pannelloCalendario.PerformLayout();
            this.calendarioMensileTableLayoutPanel.ResumeLayout(false);
            this.calendarioMensileTableLayoutPanel.PerformLayout();
            this.calendarioMeseTableLayoutPanel.ResumeLayout(false);
            this.calendarioMeseTableLayoutPanel.PerformLayout();
            this.pannelloAchievement.ResumeLayout(false);
            this.achievementSplitContainer.Panel1.ResumeLayout(false);
            this.achievementSplitContainer.Panel2.ResumeLayout(false);
            this.achievementSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.achievementSplitContainer)).EndInit();
            this.achievementSplitContainer.ResumeLayout(false);
            this.achievementDescrizioneGroupBox.ResumeLayout(false);
            this.achievementDescrizioneGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel obiettivo_pannello_iniziale;
        private System.Windows.Forms.TableLayoutPanel calendarioMensileTableLayoutPanel;
        private System.Windows.Forms.SplitContainer obiettivo_split_vista;
        private System.Windows.Forms.ComboBox obiettiviLista;
        private System.Windows.Forms.Panel pannelloStatistiche;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button statistiche_bAttivita;
        private System.Windows.Forms.Button statistiche_bSpese;
        private System.Windows.Forms.Button statistiche_bPV;
        private System.Windows.Forms.Button statistiche_bPP;
        private System.Windows.Forms.SplitContainer statisticheSplitContainer;
        private System.Windows.Forms.DataVisualization.Charting.Chart graficoStatistiche;
        private System.Windows.Forms.Button statistiche_bAnno;
        private System.Windows.Forms.Button statistiche_bMese;
        private System.Windows.Forms.Button statistiche_bSettimana;
        private System.Windows.Forms.Panel pannelloAttivita;
        private System.Windows.Forms.SplitContainer SchermInizAttività;
        private System.Windows.Forms.Button attivita_bAggiungi;
        private System.Windows.Forms.ComboBox attivita_comboSelezionaAttivita;
        private System.Windows.Forms.Button attivita_bElimina;
        private System.Windows.Forms.Button attivita_bSalva;
        private System.Windows.Forms.Button obiettivoSalvaButton;
        private System.Windows.Forms.FlowLayoutPanel attivitaFlowLayoutPanel;
        private System.Windows.Forms.GroupBox groupBox8;
        //private System.Windows.Forms.TextBox attivita_textDescrizione;
        private System.Windows.Forms.Button b_calendario;
        private System.Windows.Forms.Button b_achievement;
        private System.Windows.Forms.Button b_statistiche;
        private System.Windows.Forms.Button b_obiettivi;
        private System.Windows.Forms.Button b_attivita;
        private System.Windows.Forms.TableLayoutPanel listaBottoniSinistraTableLayoutPanel;
        private System.Windows.Forms.Button calendarioBottoneGiorno1;
        private System.Windows.Forms.Button calendarioBottoneGiorno31;
        private System.Windows.Forms.Button calendarioBottoneGiorno30;
        private System.Windows.Forms.Button calendarioBottoneGiorno29;
        private System.Windows.Forms.Button calendarioBottoneGiorno28;
        private System.Windows.Forms.Button calendarioBottoneGiorno27;
        private System.Windows.Forms.Button calendarioBottoneGiorno26;
        private System.Windows.Forms.Button calendarioBottoneGiorno25;
        private System.Windows.Forms.Button calendarioBottoneGiorno24;
        private System.Windows.Forms.Button calendarioBottoneGiorno23;
        private System.Windows.Forms.Button calendarioBottoneGiorno22;
        private System.Windows.Forms.Button calendarioBottoneGiorno21;
        private System.Windows.Forms.Button calendarioBottoneGiorno20;
        private System.Windows.Forms.Button calendarioBottoneGiorno19;
        private System.Windows.Forms.Button calendarioBottoneGiorno18;
        private System.Windows.Forms.Button calendarioBottoneGiorno17;
        private System.Windows.Forms.Button calendarioBottoneGiorno16;
        private System.Windows.Forms.Button calendarioBottoneGiorno15;
        private System.Windows.Forms.Button calendarioBottoneGiorno14;
        private System.Windows.Forms.Button calendarioBottoneGiorno13;
        private System.Windows.Forms.Button calendarioBottoneGiorno12;
        private System.Windows.Forms.Button calendarioBottoneGiorno11;
        private System.Windows.Forms.Button calendarioBottoneGiorno10;
        private System.Windows.Forms.Button calendarioBottoneGiorno9;
        private System.Windows.Forms.Button calendarioBottoneGiorno8;
        private System.Windows.Forms.Button calendarioBottoneGiorno7;
        private System.Windows.Forms.Button calendarioBottoneGiorno6;
        private System.Windows.Forms.Button calendarioBottoneGiorno5;
        private System.Windows.Forms.Button calendarioBottoneGiorno4;
        private System.Windows.Forms.Button calendarioBottoneGiorno3;
        private System.Windows.Forms.Button calendarioBottoneGiorno2;
        private System.Windows.Forms.Button calendarioBottoneGiorno35;
        private System.Windows.Forms.Button calendarioBottoneGiorno34;
        private System.Windows.Forms.Button calendarioBottoneGiorno33;
        private System.Windows.Forms.Button calendarioBottoneGiorno32;
        private System.Windows.Forms.TableLayoutPanel calendarioGiornalieroTableLayoutPanel;
        private System.Windows.Forms.Button calendarioMeseButton;
        private System.Windows.Forms.CheckedListBox calendarioAttivitaListBox;
        private System.Windows.Forms.TableLayoutPanel calendarioAttivitaTableLayoutPanel;
        private System.Windows.Forms.DateTimePicker calendarioGiornalieroDateTimePicker;
        private System.Windows.Forms.TableLayoutPanel pannelloCalendario;
        private System.Windows.Forms.Button b_Budget;
        private System.Windows.Forms.GroupBox budgetSpeseGroupBox;
        private System.Windows.Forms.GroupBox budgetBudgetGroupBox;
        private System.Windows.Forms.ComboBox budgetSpesePeriodicheComboBox;
        private System.Windows.Forms.Button budgetImpostaButton;
        private System.Windows.Forms.Label budgetImpostatoLabel;
        private System.Windows.Forms.TextBox budgetBudgetTextBox;
        private System.Windows.Forms.TableLayoutPanel pannelloBudget;
        private System.Windows.Forms.Label budgetDescrizioneLabel;
        private System.Windows.Forms.TextBox budgetDescrizioneSpesaTextBox;
        private System.Windows.Forms.Label budgetAccontoLabel;
        private System.Windows.Forms.TextBox budgetSpesaAccontoTextBox;
        private System.Windows.Forms.SplitContainer budgetSpeseSplitContainer;
        private System.Windows.Forms.TableLayoutPanel budgetBottoniTableLayoutPanel;
        private System.Windows.Forms.Button budgetAggiungiSpesaButton;
        private System.Windows.Forms.Button budgetSalvaSpesaButton;
        private System.Windows.Forms.Button budgetEliminaSpesaButton;
        private System.Windows.Forms.TextBox calendarioMeseTextBox;
        private System.Windows.Forms.TableLayoutPanel calendarioMeseTableLayoutPanel;
        private System.Windows.Forms.Button calendarioMeseBottoneSinistra;
        private System.Windows.Forms.Button calendarioMeseFrecciaDestra;
        private System.Windows.Forms.TextBox calendarioMeseGiorno1;
        private System.Windows.Forms.TextBox calendarioMeseGiorno7;
        private System.Windows.Forms.TextBox calendarioMeseGiorno6;
        private System.Windows.Forms.TextBox calendarioMeseGiorno5;
        private System.Windows.Forms.TextBox calendarioMeseGiorno4;
        private System.Windows.Forms.TextBox calendarioMeseGiorno3;
        private System.Windows.Forms.TextBox calendarioMeseGiorno2;
        private System.Windows.Forms.SplitContainer achievementSplitContainer;
        private System.Windows.Forms.ListBox achievementListBox;
        private System.Windows.Forms.Panel pannelloAchievement;
        private System.Windows.Forms.GroupBox achievementDescrizioneGroupBox;
        private System.Windows.Forms.TextBox achievementNomeTextBox;
        private System.Windows.Forms.Label achievementNomeLabel;
        private System.Windows.Forms.Label achievementDescrizioneLabel;
        private System.Windows.Forms.TextBox achievementDescrizioneTextBox;
        private System.Windows.Forms.TextBox achievementCompletamentoTextBox;
        private System.Windows.Forms.Label achievementCompletamentoLabel;
        private System.Windows.Forms.TableLayoutPanel obiettivoBottoniTableLayoutPanel;
        private System.Windows.Forms.Button obiettivoEliminaButton;
        private System.Windows.Forms.Button obiettivoAggiungiButton;
        private System.Windows.Forms.Button obiettivoVediButton;
        private System.Windows.Forms.GroupBox attivita_groupBoxNome;
        private System.Windows.Forms.TextBox attivita_textNome;
        private System.Windows.Forms.GroupBox attivita_groupBoxPremio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox attivita_textPremioPV;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox attivita_textPremioPP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox attivita_textPremioEuro;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox attivita_textCostoPV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox attivita_textCostoEuro;
        private System.Windows.Forms.GroupBox attivita_groupBoxPenitenza;
        private System.Windows.Forms.ComboBox attivita_comboPenitenza;
        private System.Windows.Forms.TextBox attivita_textPenitenza;
        private System.Windows.Forms.GroupBox attivita_groupBoxColore;
        private System.Windows.Forms.ComboBox attivita_comboColore;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox attivita_groupBoxCategoria;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelComboCategoria;
        private System.Windows.Forms.TextBox attivita_textAggiungiCategoria;
        private System.Windows.Forms.ComboBox attivita_comboCategoria;
        private System.Windows.Forms.Button attivita_bAggiungiCategoria;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

        System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1;
        private System.Windows.Forms.DateTimePicker statistiche_dateTimePicker;
        private System.Windows.Forms.Label label9;

        private System.Windows.Forms.SplitContainer obiettivoComboEGroupSplitContainer;
        private System.Windows.Forms.TableLayoutPanel obiettivoTableLayoutPanel;
        private System.Windows.Forms.GroupBox obiettivoDescrizioneGroupBox;
        private System.Windows.Forms.TextBox obiettivoDescrizioneTextBox;
        private System.Windows.Forms.GroupBox obiettivoSceltaSottoObiettiviGroupBox;
        private System.Windows.Forms.Button obiettivoAggiungiSottoObiettivoButton;
        private System.Windows.Forms.ComboBox obiettivoSceltaSottoObiettivoComboBox;
        private System.Windows.Forms.GroupBox obiettivoDurataGiorniGroupBox;
        private System.Windows.Forms.Button obiettivoAvviaButton;
        private System.Windows.Forms.Label obiettivoALabel;
        private System.Windows.Forms.Label obiettivoDaLabel;
        private System.Windows.Forms.DateTimePicker obiettivoTermineDurataDateTimePicker;
        private System.Windows.Forms.DateTimePicker obiettivoInizioDurataDateTimePicker;
        private System.Windows.Forms.GroupBox obiettivoSceltaAttivitaGroupBox;
        private System.Windows.Forms.Button obiettivoAggiungiAttivitaButton;
        private System.Windows.Forms.TextBox obiettivoNVolteTextBox;
        private System.Windows.Forms.Label obiettivoNVolteLabel;
        private System.Windows.Forms.ComboBox obiettivoSceltaAttivitaComboBox;
        private System.Windows.Forms.GroupBox attivita_groupBoxDescrizione;
        private System.Windows.Forms.TextBox attivita_textDescrizione;
        private System.Windows.Forms.GroupBox durataAttivitaGroupBox;
        private System.Windows.Forms.Label durataAttivitaALabel;
        private System.Windows.Forms.Label durataAttivitaDaLabel;
        private System.Windows.Forms.DateTimePicker attivitaADateTimePicker;
        private System.Windows.Forms.DateTimePicker attivitaDaDateTimePicker;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button attivita_bAvvia;
        private System.Windows.Forms.DateTimePicker attivitaATimePicker;
        private System.Windows.Forms.DateTimePicker attivitaDaTimePicker;
    }
}